use news_flash::models::{ArticleID, Marked, Read};
#[derive(Debug, Clone)]
pub struct ReadUpdate {
    pub article_id: ArticleID,
    pub read: Read,
}

#[derive(Debug, Clone)]
pub struct MarkUpdate {
    pub article_id: ArticleID,
    pub marked: Marked,
}
