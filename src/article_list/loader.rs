use std::collections::HashSet;

use crate::article_list::ArticleListModel;
use crate::content_page::{ArticleListMode, ContentPageState};
use crate::sidebar::{models::SidebarSelection, FeedListItemID};
use crate::undo_delete_action::UndoDelete;
use chrono::{DateTime, NaiveDateTime, TimeDelta, TimeZone, Utc};
use news_flash::error::NewsFlashError;
use news_flash::models::{Article, ArticleFilter, ArticleID, ArticleOrder, CategoryID, FeedID, Marked, Read, TagID};
use news_flash::NewsFlash;

pub struct ArticleListLoader {
    hide_future_articles: bool,
    search_term: Option<String>,
    order: ArticleOrder,
    mode: ArticleListMode,
    sidebar_selection: SidebarSelection,
    undo_delete_actions: Vec<UndoDelete>,
    last_article_date: Option<DateTime<Utc>>,
    loaded_article_ids: Vec<ArticleID>,
    force_new: bool,
}

impl ArticleListLoader {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn hide_furure_articles(mut self, hide_future_articles: bool) -> Self {
        self.hide_future_articles = hide_future_articles;
        self
    }

    pub fn search_term(mut self, search_term: Option<String>) -> Self {
        self.search_term = search_term;
        self
    }

    pub fn order(mut self, order: ArticleOrder) -> Self {
        self.order = order;
        self
    }

    pub fn mode(mut self, mode: ArticleListMode) -> Self {
        self.mode = mode;
        self
    }

    pub fn sidebar_selection(mut self, sidebar_selection: SidebarSelection) -> Self {
        self.sidebar_selection = sidebar_selection;
        self
    }

    pub fn undo_delete_actions(mut self, undo_delete_actions: Vec<UndoDelete>) -> Self {
        self.undo_delete_actions = undo_delete_actions;
        self
    }

    pub fn last_article_date(mut self, last_article_date: Option<NaiveDateTime>) -> Self {
        self.last_article_date = last_article_date.map(|date_time| Utc.from_utc_datetime(&date_time));
        self
    }

    pub fn force_new(mut self, force_new: bool) -> Self {
        self.force_new = force_new;
        self
    }

    pub fn loaded_article_ids(mut self, loaded_article_ids: Vec<ArticleID>) -> Self {
        self.loaded_article_ids = loaded_article_ids;
        self
    }

    pub fn build_update(self, news_flash: &NewsFlash) -> Result<ArticleListModel, NewsFlashError> {
        let (limit, last_article_date) = if self.force_new || self.last_article_date.is_none() {
            // article list is empty: load default amount of articles to fill it
            (Some(ContentPageState::page_size()), None)
        } else {
            (None, self.last_article_date)
        };

        let (older_than, newer_than) = if let Some(last_article_date) = last_article_date {
            match self.order {
                // +/- 1s to not exclude last article in list
                ArticleOrder::NewestFirst => (None, Some(last_article_date - TimeDelta::try_seconds(1).unwrap())),
                ArticleOrder::OldestFirst => (Some(last_article_date + TimeDelta::try_seconds(1).unwrap()), None),
            }
        } else {
            (None, None)
        };

        let article_filter = self.build_article_filter(limit, older_than, newer_than);
        let mut articles = news_flash.get_articles(article_filter)?;

        let loaded_article_count = articles.len() as i64;
        if loaded_article_count < ContentPageState::page_size() && !self.force_new {
            if let Some(last_article_date) = self.last_article_date {
                // article list is not empty but also not filled all the way up to "page size"
                // -> load a few more
                let (older_than, newer_than) = match self.order {
                    // newest first => we want articles older than the last in list
                    ArticleOrder::NewestFirst => (Some(last_article_date), None),

                    // newsest first => we want article newer than the last in list
                    ArticleOrder::OldestFirst => (None, Some(last_article_date)),
                };
                let more_articles_filter =
                    self.build_article_filter(Some(ContentPageState::page_size()), older_than, newer_than);

                let mut more_articles = news_flash.get_articles(more_articles_filter)?;
                articles.append(&mut more_articles);
            }
        }

        // if this is an update of the same unread list also load the already loaded articles
        // so they wont be removed by the update & things like the thumbnail update
        if !self.force_new && self.mode == ArticleListMode::Unread {
            if let Ok(mut a) = news_flash.get_articles(ArticleFilter::ids(self.loaded_article_ids)) {
                articles.append(&mut a);
            }
        }

        let list_model = Self::build_list_model(&self.order, news_flash, articles)?;
        Ok(list_model)
    }

    pub fn build_extend(self, news_flash: &NewsFlash) -> Result<ArticleListModel, NewsFlashError> {
        let (older_than, newer_than) = if let Some(last_article_date) = self.last_article_date {
            match self.order {
                // newest first => we want articles older than the last in list
                ArticleOrder::NewestFirst => (Some(last_article_date), None),

                // newsest first => we want article newer than the last in list
                ArticleOrder::OldestFirst => (None, Some(last_article_date)),
            }
        } else {
            (None, None)
        };

        let article_filter = self.build_article_filter(Some(ContentPageState::page_size()), older_than, newer_than);

        let articles = news_flash.get_articles(article_filter)?;
        let list_model = Self::build_list_model(&self.order, news_flash, articles)?;
        Ok(list_model)
    }

    fn build_list_model(
        order: &ArticleOrder,
        news_flash: &NewsFlash,
        mut articles: Vec<Article>,
    ) -> Result<ArticleListModel, NewsFlashError> {
        let (feeds, _feed_mappings) = news_flash.get_feeds()?;
        let (tags, taggings) = news_flash.get_tags()?;

        let articles = articles
            .drain(..)
            .map(|article| {
                let feed = feeds.iter().find(|f| f.feed_id == article.feed_id);
                let taggings: HashSet<&TagID> = taggings
                    .iter()
                    .filter(|t| t.article_id == article.article_id)
                    .map(|t| &t.tag_id)
                    .collect();
                let tags = tags.iter().filter(|t| taggings.contains(&t.tag_id)).collect::<Vec<_>>();

                (article, feed, tags)
            })
            .collect();

        let mut list_model = ArticleListModel::new(order);
        list_model.add(articles);

        Ok(list_model)
    }

    fn build_article_filter(
        &self,
        limit: Option<i64>,
        older_than: Option<DateTime<Utc>>,
        newer_than: Option<DateTime<Utc>>,
    ) -> ArticleFilter {
        // mutate older_than to hide articles in the future
        let older_than = self.should_hide_future_articles(older_than);

        let unread = match self.mode {
            ArticleListMode::All | ArticleListMode::Marked => None,
            ArticleListMode::Unread => Some(Read::Unread),
        };
        let marked = match self.mode {
            ArticleListMode::All | ArticleListMode::Unread => None,
            ArticleListMode::Marked => Some(Marked::Marked),
        };
        let (selected_feed, selected_category, selected_tag) = match &self.sidebar_selection {
            SidebarSelection::All => (None, None, None),
            SidebarSelection::Tag(id, _) => (None, None, Some(id.clone())),
            SidebarSelection::FeedList(id, _title) => match id {
                FeedListItemID::Feed(mapping) => (Some(mapping.feed_id.clone()), None, None),
                FeedListItemID::Category(id) => (None, Some(id.clone()), None),
            },
        };
        let (feed_blacklist, category_blacklist) = self.load_articles_blacklist();

        ArticleFilter {
            limit,
            offset: None,
            order: Some(self.order.clone()),
            unread,
            marked,
            feeds: selected_feed.map(|f| vec![f]),
            feed_blacklist,
            categories: selected_category.map(|c| vec![c]),
            category_blacklist,
            tags: selected_tag.map(|t| vec![t]),
            ids: None,
            newer_than,
            older_than,
            search_term: self.search_term.clone(),
        }
    }

    fn load_articles_blacklist(&self) -> (Option<Vec<FeedID>>, Option<Vec<CategoryID>>) {
        let mut feed_blacklist = Vec::new();
        let mut category_blacklist = Vec::new();

        for undo_delete_action in &self.undo_delete_actions {
            match undo_delete_action {
                UndoDelete::Feed(feed_id, _label) => feed_blacklist.push(feed_id.clone()),
                UndoDelete::Category(category_id, _label) => category_blacklist.push(category_id.clone()),
                UndoDelete::Tag(_tag_id, _label) => {}
            }
        }

        let feed_blacklist = if feed_blacklist.is_empty() {
            None
        } else {
            Some(feed_blacklist)
        };
        let category_blacklist = if category_blacklist.is_empty() {
            None
        } else {
            Some(category_blacklist)
        };

        (feed_blacklist, category_blacklist)
    }

    fn should_hide_future_articles(&self, older_than: Option<DateTime<Utc>>) -> Option<DateTime<Utc>> {
        if self.hide_future_articles {
            if let Some(older_than) = older_than {
                if older_than < Utc::now() {
                    Some(older_than)
                } else {
                    Some(Utc::now())
                }
            } else {
                Some(Utc::now())
            }
        } else {
            older_than
        }
    }
}

impl Default for ArticleListLoader {
    fn default() -> Self {
        Self {
            hide_future_articles: false,
            search_term: None,
            order: ArticleOrder::NewestFirst,
            mode: ArticleListMode::All,
            sidebar_selection: SidebarSelection::All,
            undo_delete_actions: Vec::new(),
            last_article_date: None,
            loaded_article_ids: Vec::new(),
            force_new: false,
        }
    }
}
