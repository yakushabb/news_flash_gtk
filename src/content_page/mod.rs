mod article_list_column;
mod article_list_mode;
mod article_view_column;
mod content_page_state;
mod sidebar_column;

pub use self::article_list_column::ArticleListColumn;
pub use self::article_list_mode::ArticleListMode;
pub use self::article_view_column::ArticleViewColumn;
pub use self::content_page_state::ContentPageState;
pub use self::sidebar_column::SidebarColumn;

use crate::app::App;
use crate::article_list::{ArticleListLoader, ArticleListModel};
use crate::error::NewsFlashGtkError;
use crate::i18n::{i18n, i18n_f};
use crate::main_window_actions::MainWindowActions;
use crate::responsive::ResponsiveLayout;
use crate::sidebar::models::SidebarSelection;
use crate::sidebar::{FeedListItemID, FeedListTree, SidebarLoader, TagListModel};
use crate::undo_delete_action::UndoDelete;
use crate::undo_mark_read_action::UndoMarkReadAction;
use glib::{clone, subclass};
use gtk4::{subclass::prelude::*, CompositeTemplate};
use libadwaita::{NavigationSplitView, OverlaySplitView, Toast, ToastOverlay};
use news_flash::error::NewsFlashError;
use news_flash::models::{ArticleFilter, Marked, PluginID};
use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/content_page.blp")]
    pub struct ContentPage {
        #[template_child]
        pub content_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        pub outer: TemplateChild<OverlaySplitView>,
        #[template_child]
        pub inner: TemplateChild<NavigationSplitView>,
        #[template_child]
        pub article_list_column: TemplateChild<ArticleListColumn>,
        #[template_child]
        pub sidebar_column: TemplateChild<SidebarColumn>,
        #[template_child]
        pub articleview_column: TemplateChild<ArticleViewColumn>,

        pub state: Rc<RefCell<ContentPageState>>,

        pub toasts: Rc<RefCell<HashSet<Toast>>>,
        pub current_undo_delete_action: Rc<RefCell<Option<UndoDelete>>>,
        pub processing_undo_delete_actions: Rc<RefCell<HashSet<UndoDelete>>>,
        pub current_undo_mark_read_action: Rc<RefCell<Option<UndoMarkReadAction>>>,
        pub responsive_layout: Rc<RefCell<Option<ResponsiveLayout>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ContentPage {
        const NAME: &'static str = "ContentPage";
        type ParentType = gtk4::Box;
        type Type = super::ContentPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ContentPage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ContentPage {}

    impl BoxImpl for ContentPage {}
}

glib::wrapper! {
    pub struct ContentPage(ObjectSubclass<imp::ContentPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ContentPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ContentPage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        if let Err(error) = imp.state.borrow_mut().load_from_file() {
            log::error!("Failed to deserialize the content page state: {error}");
        }

        imp.sidebar_column
            .sidebar()
            .set_subscriptions_expanded(imp.state.borrow().get_subscriptions_expanded());
        imp.sidebar_column
            .sidebar()
            .set_tags_expanded(imp.state.borrow().get_tags_expanded());

        imp.responsive_layout.borrow_mut().replace(ResponsiveLayout::new(self));
    }

    pub fn state(&self) -> Rc<RefCell<ContentPageState>> {
        self.imp().state.clone()
    }

    pub fn simple_message(&self, message: &str) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                toasts_guard.remove(toast);
            };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);
    }

    pub fn newsflash_error(&self, message: &str, error: NewsFlashError) {
        let imp = self.imp();
        let toast = Toast::new(message);
        toast.set_button_label(Some(&i18n("details")));
        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: message.into(),
        });
        toast.set_action_name(Some("win.show-error-dialog"));
        toast.connect_dismissed(clone!(@weak self as this => @default-panic, move |toast| {
            let imp = this.imp();
            if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                toasts_guard.remove(toast);
            };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);
    }

    pub fn dismiss_notifications(&self) {
        let imp = self.imp();

        for toast in imp.toasts.borrow_mut().drain() {
            toast.dismiss();
        }
    }

    pub fn remove_current_undo_delete_action(&self) {
        let imp = self.imp();
        let _ = imp.current_undo_delete_action.take();

        // update lists
        App::default().update_sidebar();
        App::default().update_article_list();
    }

    pub fn add_undo_delete_notification(&self, action: UndoDelete) {
        let imp = self.imp();

        if let Some(current_action) = imp.current_undo_delete_action.take() {
            log::debug!("remove current action: {}", current_action);
            Self::execute_action(&current_action, &imp.processing_undo_delete_actions);

            // dismiss all other toasts
            for toast in imp.toasts.borrow_mut().drain() {
                toast.dismiss();
            }
        }

        let message = match &action {
            UndoDelete::Category(_id, label) => i18n_f("Deleted Category '{}'", &[label]),
            UndoDelete::Feed(_id, label) => i18n_f("Deleted Feed '{}'", &[label]),
            UndoDelete::Tag(_id, label) => i18n_f("Deleted Tag '{}'", &[label]),
        };

        let toast = Toast::new(&message);
        toast.set_button_label(Some(&i18n("undo")));
        toast.set_action_name(Some("win.remove-undo-action"));
        toast.connect_dismissed(clone!(
            @strong action,
            @weak self as this => @default-panic, move |toast| {
                let imp = this.imp();

                if let Some(current_action) = imp.current_undo_delete_action.take() {
                    log::debug!("remove current action: {}", current_action);
                    Self::execute_action(&current_action, &imp.processing_undo_delete_actions);
                };

                if let Ok(mut toasts_guard) = imp.toasts.try_borrow_mut() {
                    toasts_guard.remove(toast);
                };
        }));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);

        imp.current_undo_delete_action.replace(Some(action));

        // update lists
        App::default().update_sidebar();
        App::default().update_article_list();
    }

    pub fn execute_pending_undoable_action(&self) {
        let imp = self.imp();

        if let Some(current_action) = self.get_current_undo_delete_action() {
            Self::execute_action(&current_action, &imp.processing_undo_delete_actions);
        }
    }

    pub fn get_current_undo_delete_action(&self) -> Option<UndoDelete> {
        self.imp().current_undo_delete_action.borrow().clone()
    }

    pub fn processing_undo_delete_actions(&self) -> Rc<RefCell<HashSet<UndoDelete>>> {
        self.imp().processing_undo_delete_actions.clone()
    }

    fn execute_action(action: &UndoDelete, processing_actions: &Rc<RefCell<HashSet<UndoDelete>>>) {
        processing_actions.borrow_mut().insert(action.clone());
        let callback = Box::new(
            clone!(@strong processing_actions, @strong action => @default-panic, move || {
                processing_actions.borrow_mut().remove(&action);
            }),
        );
        match action {
            UndoDelete::Feed(feed_id, _label) => {
                App::default().delete_feed(feed_id.clone(), callback);
            }
            UndoDelete::Category(category_id, _label) => {
                App::default().delete_category(category_id.clone(), callback);
            }
            UndoDelete::Tag(tag_id, _label) => {
                App::default().delete_tag(tag_id.clone(), callback);
            }
        }
    }

    pub fn add_undo_mark_read_notification(&self, action: UndoMarkReadAction) {
        let imp = self.imp();

        // dismiss all other toasts
        for toast in imp.toasts.borrow_mut().drain() {
            toast.dismiss();
        }

        let toast = Toast::new(&i18n("Mark all read"));
        toast.set_button_label(Some(&i18n("undo")));
        toast.set_action_name(Some("win.undo-mark-all-read"));
        imp.content_overlay.add_toast(toast.clone());
        imp.toasts.borrow_mut().insert(toast);

        imp.current_undo_mark_read_action.replace(Some(action));
    }

    pub fn set_sidebar_read(&self) {
        self.article_list_column().start_mark_all_read();

        let sidebar_selection = self.state().borrow().get_sidebar_selection().clone();
        let search_term = self.state().borrow().get_search_term().map(|s| s.to_string());
        let list_mode = self.state().borrow().get_article_list_mode();

        if search_term.is_none() && list_mode != ArticleListMode::Marked {
            match sidebar_selection {
                SidebarSelection::All => App::set_all_read(),
                SidebarSelection::FeedList(item_id, _title) => match &item_id {
                    FeedListItemID::Feed(feed_mapping) => App::set_feed_read(&feed_mapping.feed_id),
                    FeedListItemID::Category(category_id) => App::set_category_read(category_id),
                },
                SidebarSelection::Tag(tag_id, _title) => App::set_tag_read(&tag_id),
            }
        } else {
            let mut filter = ArticleFilter {
                search_term,
                marked: match list_mode {
                    ArticleListMode::All => None,
                    ArticleListMode::Unread => None,
                    ArticleListMode::Marked => Some(Marked::Marked),
                },
                ..Default::default()
            };
            match sidebar_selection {
                SidebarSelection::All => {}
                SidebarSelection::FeedList(item_id, _title) => match &item_id {
                    FeedListItemID::Feed(feed_mapping) => filter.feeds = Some(vec![feed_mapping.feed_id.clone()]),
                    FeedListItemID::Category(category_id) => filter.categories = Some(vec![category_id.clone()]),
                },
                SidebarSelection::Tag(tag_id, _title) => filter.tags = Some(vec![tag_id]),
            }
            App::set_filter_read(filter)
        }
    }

    pub fn sidebar_column(&self) -> &SidebarColumn {
        let imp = self.imp();
        &imp.sidebar_column
    }

    pub fn article_list_column(&self) -> &ArticleListColumn {
        let imp = self.imp();
        &imp.article_list_column
    }

    pub fn articleview_column(&self) -> &ArticleViewColumn {
        let imp = self.imp();
        &imp.articleview_column
    }

    pub fn outer(&self) -> &OverlaySplitView {
        let imp = self.imp();
        &imp.outer
    }

    pub fn inner(&self) -> &NavigationSplitView {
        let imp = self.imp();
        &imp.inner
    }

    pub fn responsive_layout(&self) -> ResponsiveLayout {
        let imp = self.imp();
        imp.responsive_layout
            .borrow()
            .clone()
            .expect("ContentPage not initialized")
    }

    pub fn load_branding(&self) {
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    (news_flash.id().await, news_flash.user_name().await)
                } else {
                    (None, None)
                }
            },
            |app, (id, user)| {
                if let Some(id) = id {
                    if app
                        .main_window()
                        .content_page()
                        .load_branding_impl(&id, user.as_deref())
                    {
                        // try to fill content page with data
                        app.update_sidebar();
                        app.update_article_list();
                    }
                } else {
                    // in case of failure show 'welcome page'
                    app.main_window().show_welcome_page();
                }
            },
        );
    }

    fn load_branding_impl(&self, id: &PluginID, user_name: Option<&str>) -> bool {
        if self.sidebar_column().set_account(id, user_name).is_err() {
            App::default().in_app_notifiaction(&i18n("Failed to set service logo"));
            false
        } else {
            true
        }
    }

    pub fn clear(&self) {
        let settings = App::default().settings();
        self.articleview_column().article_view().close_article();
        self.state().borrow_mut().set_prefer_scraped_content(false);

        let empty_list_model = ArticleListModel::new(&settings.read().get_article_list_order());
        self.article_list_column().article_list().update(empty_list_model, true);

        let feed_tree_model = FeedListTree::new();
        self.sidebar_column().sidebar().update_feedlist(feed_tree_model);

        let tag_list_model = TagListModel::new();
        self.sidebar_column().sidebar().update_taglist(tag_list_model);
        self.sidebar_column().sidebar().hide_taglist();
    }

    pub fn update_article_list(&self) {
        self.update_article_list_force(false);
    }

    pub fn update_article_list_forced(&self) {
        self.update_article_list_force(true);
    }

    fn update_article_list_force(&self, force_new: bool) {
        let loader = ArticleListLoader::new()
            .hide_furure_articles(App::default().settings().read().get_article_list_hide_future_articles())
            .search_term(self.state().borrow().get_search_term().map(String::from))
            .order(App::default().settings().read().get_article_list_order())
            .mode(self.state().borrow().get_article_list_mode())
            .sidebar_selection(self.state().borrow().get_sidebar_selection().clone())
            .undo_delete_actions(self.undo_delete_actions())
            .last_article_date(
                self.article_list_column()
                    .article_list()
                    .get_last_row_model()
                    .map(|model| model.date),
            )
            .force_new(force_new)
            .loaded_article_ids(self.article_list_column().article_list().loaded_article_ids());

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let new_list_model = loader.build_update(news_flash)?;
                    Ok(new_list_model)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Ok(article_list_model) = res {
                    app.main_window()
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .update(article_list_model, force_new);
                }
            },
        );
    }

    pub fn load_more_articles(&self) {
        let loader = ArticleListLoader::new()
            .hide_furure_articles(App::default().settings().read().get_article_list_hide_future_articles())
            .search_term(self.state().borrow().get_search_term().map(String::from))
            .order(App::default().settings().read().get_article_list_order())
            .mode(self.state().borrow().get_article_list_mode())
            .sidebar_selection(self.state().borrow().get_sidebar_selection().clone())
            .undo_delete_actions(self.undo_delete_actions())
            .last_article_date(
                self.article_list_column()
                    .article_list()
                    .get_last_row_model()
                    .map(|model| model.date),
            )
            .loaded_article_ids(self.article_list_column().article_list().loaded_article_ids());

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let list_model = loader.build_extend(news_flash)?;
                    Ok(list_model)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| {
                if let Ok(article_list_model) = res {
                    app.main_window()
                        .content_page()
                        .article_list_column()
                        .article_list()
                        .add_more_articles(article_list_model);
                }
            },
        );
    }

    fn undo_delete_actions(&self) -> Vec<UndoDelete> {
        let current_undo_delete_action = self.get_current_undo_delete_action();
        let processing_undo_delete_actions = self.processing_undo_delete_actions();

        let mut undo_delete_actions = Vec::new();

        if let Some(current_undo_delete_action) = current_undo_delete_action {
            undo_delete_actions.push(current_undo_delete_action);
        }

        for processing_undo_delete_action in &*processing_undo_delete_actions.borrow() {
            undo_delete_actions.push(processing_undo_delete_action.clone());
        }

        undo_delete_actions
    }

    pub fn update_sidebar(&self) {
        let loader = SidebarLoader::new()
            .hide_future_articles(App::default().settings().read().get_article_list_hide_future_articles())
            .article_list_mode(self.state().borrow().get_article_list_mode())
            .undo_delete_actions(self.undo_delete_actions());

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    loader.load(news_flash)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| match res {
                Ok((total_count, feed_list_model, tag_list_model)) => {
                    let sidebar = app.main_window().content_page().sidebar_column().sidebar().clone();

                    sidebar.update_all(total_count);
                    sidebar.update_feedlist(feed_list_model);

                    if tag_list_model.is_empty() {
                        sidebar.hide_taglist();
                    } else {
                        sidebar.show_taglist();
                    }

                    sidebar.update_taglist(tag_list_model);
                }
                Err(error) => {
                    App::default().in_app_notifiaction(&i18n_f("Failed to update sidebar: '{}'", &[&error.to_string()]))
                }
            },
        );
    }

    pub fn article_view_scroll_diff(&self, diff: f64) {
        self.articleview_column().article_view().animate_scroll_diff(diff)
    }

    pub fn sidebar_select_next_item(&self) {
        self.sidebar_column().sidebar().select_next_item()
    }

    pub fn sidebar_select_prev_item(&self) {
        self.sidebar_column().sidebar().select_prev_item()
    }

    pub fn set_offline(&self, offline: bool) {
        self.state().borrow_mut().set_offline(offline);
        self.articleview_column().set_offline(offline);
        self.sidebar_column().set_offline(offline);

        MainWindowActions::update_state();
    }
}
