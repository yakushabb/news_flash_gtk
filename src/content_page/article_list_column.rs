use super::article_list_mode::ArticleListMode;
use crate::app::App;
use crate::article_list::ArticleList;
use crate::main_window_actions::MainWindowActions;
use glib::{clone, subclass, ControlFlow, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, SearchBar, SearchEntry, Stack, StackTransitionType,
    ToggleButton,
};
use libadwaita::{HeaderBar, ViewStack};
use std::cell::RefCell;
use std::rc::Rc;
use std::time::Duration;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_list/column.blp")]
    pub struct ArticleListColumn {
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub update_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub search_bar: TemplateChild<SearchBar>,
        #[template_child]
        pub mark_all_read_stack: TemplateChild<Stack>,
        #[template_child]
        pub show_sidebar_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub view_switcher_stack: TemplateChild<ViewStack>,
        #[template_child]
        pub article_list: TemplateChild<ArticleList>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleListColumn {
        const NAME: &'static str = "ArticleListColumn";
        type ParentType = gtk4::Box;
        type Type = super::ArticleListColumn;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleListColumn {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ArticleListColumn {}

    impl BoxImpl for ArticleListColumn {}
}

glib::wrapper! {
    pub struct ArticleListColumn(ObjectSubclass<imp::ArticleListColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleListColumn {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleListColumn {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.article_list.init(&imp.search_bar);

        self.setup_view_switcher();
        Self::setup_search_button(&imp.search_button, &imp.search_bar);
        Self::setup_search_bar(&imp.search_bar, &imp.search_button, &imp.search_entry);
        Self::setup_search_entry(&imp.search_entry);
    }

    pub fn article_list(&self) -> &ArticleList {
        let imp = self.imp();
        &imp.article_list
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = self.imp();
        &imp.headerbar
    }

    pub fn show_sidebar_button(&self) -> &ToggleButton {
        let imp = self.imp();
        &imp.show_sidebar_button
    }

    pub fn update_stack(&self) -> &Stack {
        let imp = self.imp();
        &imp.update_stack
    }

    pub fn set_view_switcher_stack(&self, mode: ArticleListMode) {
        let imp = self.imp();

        let mode_str = match mode {
            ArticleListMode::All => "all",
            ArticleListMode::Unread => "unread",
            ArticleListMode::Marked => "marked",
        };
        imp.view_switcher_stack
            .set_visible_child_name(&format!("{mode_str}_placeholder"));
    }

    pub fn set_search_term(&self, search_term: &str) {
        let imp = self.imp();
        imp.search_button.set_active(true);
        imp.search_entry.set_text(search_term);
    }

    fn setup_view_switcher(&self) {
        let imp = self.imp();

        let view_switcher_timeout: Rc<RefCell<Option<SourceId>>> = Rc::new(RefCell::new(None));
        let article_list_mode = Rc::new(RefCell::new(ArticleListMode::All));

        imp.view_switcher_stack.connect_visible_child_name_notify(clone!(
            @strong article_list_mode,
            @strong view_switcher_timeout,
            @strong self as column => @default-panic, move |stack|
        {
            if let Some(child_name) = stack.visible_child_name() {
                let child_name = child_name.to_string();

                article_list_mode.replace(if child_name.starts_with("unread") {
                    ArticleListMode::Unread
                } else if child_name.starts_with("marked") {
                    ArticleListMode::Marked
                } else {
                    ArticleListMode::All
                });

                if view_switcher_timeout.borrow_mut().is_some() {
                    return;
                }

                column.stack_switched(&article_list_mode, &view_switcher_timeout);
            }
        }));
    }

    fn stack_switched(
        &self,
        article_list_mode: &Rc<RefCell<ArticleListMode>>,
        view_switcher_timeout: &Rc<RefCell<Option<SourceId>>>,
    ) {
        self.set_mode(*article_list_mode.borrow());

        if view_switcher_timeout.borrow().is_some() {
            return;
        }

        let mode_before_cooldown = *article_list_mode.borrow();
        view_switcher_timeout.replace(Some(glib::timeout_add_local(
            Duration::from_millis(250),
            clone!(
                @strong article_list_mode,
                @strong view_switcher_timeout,
                @strong self as column => @default-panic, move ||
            {
                view_switcher_timeout.take();
                if mode_before_cooldown != *article_list_mode.borrow() {
                    column.stack_switched(
                        &article_list_mode,
                        &view_switcher_timeout,
                    );
                }
                ControlFlow::Break
            }),
        )));
    }

    fn set_mode(&self, new_mode: ArticleListMode) {
        let content_page_state = App::default().content_page_state();
        let old_mode = content_page_state.borrow().get_article_list_mode();
        content_page_state.borrow_mut().set_article_list_mode(new_mode);

        let transition = self.calc_transition_type(&new_mode, &old_mode);
        self.article_list().set_transition(transition);

        self.set_view_switcher_stack(new_mode);
        App::default()
            .main_window()
            .content_page()
            .update_article_list_force(true);

        let update_sidebar = match old_mode {
            ArticleListMode::All | ArticleListMode::Unread => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => false,
                ArticleListMode::Marked => true,
            },
            ArticleListMode::Marked => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => true,
                ArticleListMode::Marked => false,
            },
        };
        if update_sidebar {
            App::default().update_sidebar();
        }
    }

    fn calc_transition_type(&self, new_mode: &ArticleListMode, old_mode: &ArticleListMode) -> StackTransitionType {
        match old_mode {
            ArticleListMode::All => match new_mode {
                ArticleListMode::All => {}
                ArticleListMode::Unread | ArticleListMode::Marked => return StackTransitionType::SlideLeft,
            },
            ArticleListMode::Unread => match new_mode {
                ArticleListMode::All => return StackTransitionType::SlideRight,
                ArticleListMode::Unread => {}
                ArticleListMode::Marked => return StackTransitionType::SlideLeft,
            },
            ArticleListMode::Marked => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => return StackTransitionType::SlideRight,
                ArticleListMode::Marked => {}
            },
        }

        StackTransitionType::Crossfade
    }

    fn setup_search_button(search_button: &ToggleButton, search_bar: &SearchBar) {
        search_button.connect_toggled(clone!(@weak search_bar => @default-panic, move |button| {
            if button.is_active() {
                search_bar.set_search_mode(true);
            } else {
                search_bar.set_search_mode(false);
            }
        }));
    }

    fn setup_search_bar(search_bar: &SearchBar, search_button: &ToggleButton, search_entry: &SearchEntry) {
        search_bar.connect_entry(search_entry);
        search_bar.connect_search_mode_enabled_notify(
            clone!(@weak search_button => @default-panic, move |search_bar| {
                if !search_bar.is_search_mode() {
                    search_button.set_active(false);
                }
            }),
        );
    }

    fn setup_search_entry(search_entry: &SearchEntry) {
        search_entry.connect_search_changed(|search_entry| {
            let search_term = search_entry.text().as_str().to_string();
            let empty_search = search_term.is_empty();

            let update_article_list =
                if let Some(current_search_term) = App::default().content_page_state().borrow().get_search_term() {
                    current_search_term != search_term
                } else {
                    !empty_search
                };

            let search_term = if empty_search { None } else { Some(search_term) };

            App::default()
                .main_window()
                .content_page()
                .state()
                .borrow_mut()
                .set_search_term(search_term);

            if update_article_list {
                App::default()
                    .main_window()
                    .content_page()
                    .update_article_list_force(true);
            }
        });
        // workaround to get 'has_focus' of search entry
        let entry = search_entry.first_child().unwrap().next_sibling().unwrap();
        entry.connect_has_focus_notify(|entry| {
            App::default()
                .content_page_state()
                .borrow_mut()
                .set_search_focused(entry.has_focus());
            MainWindowActions::update_state();
        });
    }

    pub fn start_sync(&self) {
        let imp = self.imp();
        imp.update_stack.set_visible_child_name("spinner");
    }

    pub fn finish_sync(&self) {
        let imp = self.imp();
        imp.update_stack.set_visible_child_name("button");
    }

    pub fn start_mark_all_read(&self) {
        let imp = self.imp();
        imp.mark_all_read_stack.set_visible_child_name("spinner");
    }

    pub fn finish_mark_all_read(&self) {
        let imp = self.imp();
        imp.mark_all_read_stack.set_visible_child_name("button");
    }

    pub fn focus_search(&self) {
        let imp = self.imp();
        // shortcuts ignored when focues -> no need to hide seach bar on keybind (ESC still works)
        imp.search_button.set_active(true);
        imp.search_entry.grab_focus();
    }
}
