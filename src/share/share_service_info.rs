use once_cell::sync::Lazy;

pub static POCKET: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-pocket",
    name: "Pocket",
    url_template: "https://getpocket.com/save?url=${url}&title=${title}",
});

pub static INSTAPAPER: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-instapaper",
    name: "Instapaper",
    url_template: "http://www.instapaper.com/hello2?url=${url}&title=${title}",
});

pub static TWITTER: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-twitter",
    name: "Twitter",
    url_template: "https://twitter.com/intent/tweet?url=${url}&text=${title}",
});

pub static MASTODON: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-mastodon",
    name: "Mastodon",
    url_template: "https://sharetomastodon.github.io/?title=${title}&url=${url}",
});

pub static REDDIT: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-reddit",
    name: "Reddit",
    url_template: "http://www.reddit.com/submit?url=${url}&title=${title}",
});

pub static TELEGRAM: Lazy<ShareServiceInfo> = Lazy::new(|| ShareServiceInfo {
    icon_name: "share-telegram",
    name: "Telegram",
    url_template: "tg://msg_url?url=${url}&text=${title}",
});

#[derive(Clone, Debug)]
pub struct ShareServiceInfo {
    pub icon_name: &'static str,
    pub name: &'static str,
    pub url_template: &'static str,
}
