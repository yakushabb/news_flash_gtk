use glib::prelude::*;
use glib::{clone, subclass};
use gtk4::{subclass::prelude::*, CompositeTemplate, Widget, Window};
use libadwaita::prelude::*;
use libadwaita::{subclass::prelude::AdwWindowImpl, EntryRow, Window as AdwWindow};
use news_flash::models::Category;

use crate::app::App;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/edit_dialogs/category.blp")]
    pub struct EditCategoryDialog {
        #[template_child]
        pub category_name: TemplateChild<EntryRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EditCategoryDialog {
        const NAME: &'static str = "EditCategoryDialog";
        type Type = super::EditCategoryDialog;
        type ParentType = AdwWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EditCategoryDialog {}

    impl WidgetImpl for EditCategoryDialog {}

    impl WindowImpl for EditCategoryDialog {}

    impl AdwWindowImpl for EditCategoryDialog {}
}

glib::wrapper! {
    pub struct EditCategoryDialog(ObjectSubclass<imp::EditCategoryDialog>)
        @extends Widget, Window, AdwWindow;
}

impl EditCategoryDialog {
    pub fn new<W: IsA<Window>>(parent: &W, category: Category) -> Self {
        let dialog: EditCategoryDialog = glib::Object::new();
        let imp = dialog.imp();
        imp.category_name.set_text(&category.label);

        imp.category_name
            .connect_apply(clone!(@strong category => @default-panic, move |entry| {
                App::default().rename_category(&category.category_id, entry.text().as_str());
            }));

        dialog.set_transient_for(Some(parent));
        dialog.present();
        dialog
    }
}
