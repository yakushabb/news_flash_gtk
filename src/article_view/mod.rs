mod models;
mod progress_overlay;
mod url_overlay;

pub use self::models::ArticleTheme;
use self::progress_overlay::ProgressOverlay;
use self::url_overlay::UrlOverlay;
use crate::app::{App, WEBKIT_DATA_DIR};
use crate::image_dialog::ImageDialog;
use crate::main_window_actions::MainWindowActions;
use crate::self_stack::SelfStack;
use crate::util::{constants, DateUtil, GtkUtil, CHANNEL_ERROR, GTK_RESOURCE_FILE_ERROR};
use chrono::{DateTime, TimeDelta, Utc};
use eyre::{eyre, Result};
use futures::channel::oneshot::Sender as OneShotSender;
use gdk4::{Key, ModifierType, ScrollDirection, ScrollEvent, RGBA};
use gio::Cancellable;
use glib::{clone, subclass, ControlFlow, MainLoop, Object, Propagation, SignalHandlerId, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Align, Box, CompositeTemplate, EventControllerKey, EventControllerMotion,
    EventControllerScroll, EventSequenceState, GestureDrag, Overlay, Stack, TickCallbackId, Widget,
};
use log::{error, warn};
use news_flash::models::{Enclosure, FatArticle, Marked, Read, Url};
use pango::FontDescription;
use rust_embed::RustEmbed;
use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::str;
use std::time::Duration;
use url::{Host, Origin};
use webkit6::{
    prelude::*, CacheModel, ContextMenuAction, ContextMenuItem, LoadEvent, NavigationPolicyDecision, NetworkProxyMode,
    NetworkProxySettings, NetworkSession, PolicyDecisionType, Settings as WebkitSettings, UserContentFilterStore,
    UserContentInjectedFrames, UserScript, UserScriptInjectionTime, UserStyleLevel, UserStyleSheet, WebContext,
    WebView, WebsiteDataTypes,
};

#[derive(RustEmbed)]
#[folder = "data/resources/article_view"]
struct ArticleViewResources;

#[derive(Clone, Debug)]
pub struct ScrollAnimationProperties {
    pub start_time: Rc<RefCell<Option<i64>>>,
    pub end_time: Rc<RefCell<Option<i64>>>,
    pub scroll_callback_id: Rc<RefCell<Option<TickCallbackId>>>,
    pub transition_start_value: Rc<RefCell<Option<f64>>>,
    pub transition_diff: Rc<RefCell<Option<f64>>>,
}

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_view/view.blp")]
    pub struct ArticleView {
        #[template_child]
        pub progress_overlay: TemplateChild<Overlay>,
        #[template_child]
        pub url_overlay: TemplateChild<Overlay>,

        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub self_stack: TemplateChild<SelfStack>,
        #[template_child]
        pub empty_status: TemplateChild<libadwaita::StatusPage>,

        pub view: RefCell<WebView>,
        pub web_context: RefCell<WebContext>,
        pub network_session: RefCell<NetworkSession>,

        #[template_child]
        pub scroll_event: TemplateChild<EventControllerScroll>,
        #[template_child]
        pub key_event: TemplateChild<EventControllerKey>,
        #[template_child]
        pub motion_event: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub drag_gesture: TemplateChild<GestureDrag>,

        #[template_child]
        pub url_overlay_label: TemplateChild<UrlOverlay>,
        #[template_child]
        pub progress_overlay_label: TemplateChild<ProgressOverlay>,

        pub visible_article: Rc<RefCell<Option<FatArticle>>>,
        pub visible_feed_name: RefCell<Option<String>>,
        pub visible_article_enclosures: RefCell<Option<Vec<Enclosure>>>,
        pub visible_article_shows_scraped_content: Cell<bool>,
        pub image_dialog_visible: Rc<Cell<bool>>,
        pub curser_over_link: Cell<bool>,

        pub load_start_timestamp: RefCell<DateTime<Utc>>,
        pub remember_toolbar_state: Rc<RefCell<(bool, bool)>>,

        pub decide_policy_signal: RefCell<Option<SignalHandlerId>>,
        pub mouse_over_signal: RefCell<Option<SignalHandlerId>>,
        pub ctx_menu_signal: RefCell<Option<SignalHandlerId>>,
        pub load_progress_signal: RefCell<Option<SignalHandlerId>>,
        pub load_changed_signal: RefCell<Option<SignalHandlerId>>,
        pub unfreeze_insurance_source: Rc<RefCell<Option<SourceId>>>,

        pub drag_ongoing: Cell<bool>,
        pub drag_buffer: RefCell<[f64; 10]>,
        pub drag_y_offset: Cell<f64>,
        pub drag_momentum: Cell<f64>,
        pub pointer_pos: Cell<(f64, f64)>,
        pub drag_buffer_update_signal: RefCell<Option<SourceId>>,
        pub drag_released_motion_signal: RefCell<Option<SourceId>>,
        pub scroll_animation_data: ScrollAnimationProperties,
    }

    impl Default for ArticleView {
        fn default() -> Self {
            let network_session = NetworkSession::new(WEBKIT_DATA_DIR.to_str(), WEBKIT_DATA_DIR.to_str());

            let proxies = App::default().settings().read().get_proxy();
            if !proxies.is_empty() {
                let mut proxy_settings = NetworkProxySettings::new(None, &[]);
                for proxy in proxies {
                    match proxy.protocoll {
                        crate::settings::ProxyProtocoll::All => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url);
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url);
                        }
                        crate::settings::ProxyProtocoll::Http => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url)
                        }
                        crate::settings::ProxyProtocoll::Https => {
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url)
                        }
                    }
                }
                network_session.set_proxy_settings(NetworkProxyMode::Custom, Some(&proxy_settings));
            }

            let web_context = WebContext::new();
            web_context.set_cache_model(CacheModel::DocumentBrowser);

            log::info!(
                "WebKit version: {}.{}.{}",
                webkit6::functions::major_version(),
                webkit6::functions::minor_version(),
                webkit6::functions::micro_version()
            );

            Self {
                progress_overlay: TemplateChild::default(),
                url_overlay: TemplateChild::default(),
                stack: TemplateChild::default(),
                self_stack: TemplateChild::default(),
                empty_status: TemplateChild::default(),

                load_start_timestamp: RefCell::new(Utc::now()),
                remember_toolbar_state: Rc::new(RefCell::new((false, false))),

                view: RefCell::new(super::ArticleView::new_webview(&web_context, &network_session)),
                web_context: RefCell::new(web_context),
                network_session: RefCell::new(network_session),

                scroll_event: TemplateChild::default(),
                key_event: TemplateChild::default(),
                motion_event: TemplateChild::default(),
                drag_gesture: TemplateChild::default(),

                url_overlay_label: TemplateChild::default(),
                progress_overlay_label: TemplateChild::default(),

                visible_article: Rc::new(RefCell::new(None)),
                visible_feed_name: RefCell::new(None),
                visible_article_enclosures: RefCell::new(None),
                visible_article_shows_scraped_content: Cell::new(false),
                image_dialog_visible: Rc::new(Cell::new(false)),
                curser_over_link: Cell::new(false),

                decide_policy_signal: RefCell::new(None),
                mouse_over_signal: RefCell::new(None),
                ctx_menu_signal: RefCell::new(None),
                load_progress_signal: RefCell::new(None),
                load_changed_signal: RefCell::new(None),
                unfreeze_insurance_source: Rc::new(RefCell::new(None)),

                drag_ongoing: Cell::new(false),
                drag_buffer: RefCell::new([0.0; 10]),
                drag_y_offset: Cell::new(0.0),
                drag_momentum: Cell::new(0.0),
                pointer_pos: Cell::new((0.0, 0.0)),
                drag_buffer_update_signal: RefCell::new(None),
                drag_released_motion_signal: RefCell::new(None),
                scroll_animation_data: ScrollAnimationProperties {
                    start_time: Rc::new(RefCell::new(None)),
                    end_time: Rc::new(RefCell::new(None)),
                    scroll_callback_id: Rc::new(RefCell::new(None)),
                    transition_start_value: Rc::new(RefCell::new(None)),
                    transition_diff: Rc::new(RefCell::new(None)),
                },
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleView {
        const NAME: &'static str = "ArticleView";
        type ParentType = Box;
        type Type = super::ArticleView;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleView {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ArticleView {}

    impl BoxImpl for ArticleView {}
}

glib::wrapper! {
    pub struct ArticleView(ObjectSubclass<imp::ArticleView>)
        @extends Widget, Box;
}

impl Default for ArticleView {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleView {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.self_stack.set_transition_duration(200);
        imp.self_stack.set_widget(&*imp.view.borrow());

        self.load_user_data();
        self.connect_event_controller();
        self.connect_webview();

        imp.view.borrow().load_html("", None);
        imp.view.borrow().set_sensitive(false);
        imp.stack.set_visible_child_name("empty");
    }

    pub fn show_article(&self, article: FatArticle, feed_name: String, enclosures: Option<Vec<Enclosure>>) {
        let imp = self.imp();

        // restore saved zoom the first time loading an article
        if let Some(zoom) = App::default().content_page_state().borrow_mut().get_article_view_zoom() {
            imp.view.borrow().set_zoom_level(zoom);
        }

        let is_new_article = if let Some(visible_article) = imp.visible_article.borrow().as_ref() {
            let visible_article_has_scraped_content = visible_article.scraped_content.is_some();
            let new_article_has_scraped_content = article.scraped_content.is_some();
            let scraped_content_visible = imp.visible_article_shows_scraped_content.get();

            visible_article.article_id != article.article_id
                || imp.visible_feed_name.borrow().as_ref() != Some(&feed_name)
                || visible_article_has_scraped_content != new_article_has_scraped_content
                || scraped_content_visible
                    != App::default()
                        .content_page_state()
                        .borrow()
                        .get_prefer_scraped_content()
        } else {
            true
        };

        Self::stop_scroll_animation(&imp.view.borrow(), &imp.scroll_animation_data);

        imp.url_overlay_label.set_url(None);
        imp.progress_overlay_label.reveal(false);

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        if is_new_article {
            if !self.is_empty() {
                self.freeze_view();
            }

            Self::set_scroll_pos_static(&imp.view.borrow(), 0.0, false);
            imp.load_start_timestamp.replace(Utc::now());

            let html = self.build_article(&article, &feed_name);
            let base_url = Self::get_base_url(&article);

            imp.visible_article.replace(Some(article));
            imp.visible_feed_name.replace(Some(feed_name));
            imp.visible_article_enclosures.replace(enclosures);

            imp.view.borrow().load_html(&html, base_url.as_deref());
            imp.view.borrow().set_sensitive(true);

            if self.is_empty() {
                imp.stack.set_visible_child_name("article");
            }
        }

        self.grab_focus();
    }

    pub fn redraw_article(&self) {
        let imp = self.imp();

        if self.is_empty() || imp.visible_article.borrow().is_none() || imp.visible_feed_name.borrow().is_none() {
            warn!("Can't redraw article view. No article is on display.");
            return;
        }

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        if let Some(article) = imp.visible_article.borrow().as_ref() {
            if let Some(feed_name) = imp.visible_feed_name.borrow().as_deref() {
                self.freeze_view();

                imp.load_start_timestamp.replace(Utc::now());
                let html = self.build_article(article, feed_name);
                imp.view
                    .borrow()
                    .load_html(&html, Self::get_base_url(article).as_deref());
                imp.view.borrow().set_sensitive(true);
            }
        }
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.stack
            .visible_child_name()
            .map(|s| s.as_str() != "article")
            .unwrap_or(false)
    }

    fn freeze_view(&self) {
        let imp = self.imp();
        let self_stack = imp.self_stack.clone();
        let unfreeze_insurance_source = imp.unfreeze_insurance_source.clone();

        self_stack.freeze();

        imp.unfreeze_insurance_source
            .replace(Some(glib::timeout_add_local(Duration::from_millis(1500), move || {
                unfreeze_insurance_source.replace(None);
                if self_stack.is_frozen() {
                    self_stack.update(gtk4::StackTransitionType::Crossfade);
                }
                ControlFlow::Break
            })));
    }

    fn get_base_url(article: &FatArticle) -> Option<String> {
        if let Some(url) = &article.url {
            match url.origin() {
                Origin::Opaque(_op) => None,
                Origin::Tuple(scheme, host, port) => {
                    let host = match host {
                        Host::Domain(domain) => domain,
                        Host::Ipv4(ipv4) => ipv4.to_string(),
                        Host::Ipv6(ipv6) => ipv6.to_string(),
                    };
                    Some(format!("{}://{}:{}", scheme, host, port))
                }
            }
        } else {
            None
        }
    }

    pub fn get_visible_article(&self) -> (Option<FatArticle>, Option<Vec<Enclosure>>) {
        let imp = self.imp();
        (
            (*imp.visible_article.borrow()).clone(),
            imp.visible_article_enclosures.borrow().clone(),
        )
    }

    pub fn update_visible_article(&self, read: Option<Read>, marked: Option<Marked>) {
        let imp = self.imp();
        if let Some(visible_article) = &mut *imp.visible_article.borrow_mut() {
            if let Some(marked) = marked {
                visible_article.marked = marked;
            }
            if let Some(read) = read {
                visible_article.unread = read;
            }
        }
    }

    pub fn close_article(&self) {
        let imp = self.imp();
        imp.view.borrow().set_sensitive(false);
        imp.view.borrow().load_html("", None);
        imp.url_overlay_label.set_url(None);
        imp.progress_overlay_label.reveal(false);
        imp.visible_article.take();
        imp.visible_feed_name.take();
        imp.stack.set_visible_child_name("empty");

        App::default()
            .content_page_state()
            .borrow_mut()
            .set_visible_article_id(None);
        MainWindowActions::update_state();
    }

    pub fn update_background_color(&self, color: &RGBA) {
        self.imp().view.borrow().set_background_color(color);
        self.redraw_article();
    }

    fn on_crash(&self) {
        let imp = self.imp();

        //hide overlays
        imp.progress_overlay_label.reveal(false);
        imp.url_overlay_label.set_url(None);

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        // disconnect webview signals
        self.disconnect_view_signals();

        // replace webview
        let new_view = Self::new_webview(&imp.web_context.borrow(), &imp.network_session.borrow());
        imp.self_stack.set_widget(&new_view);
        imp.view.replace(new_view);
        self.connect_webview();

        imp.stack.set_visible_child_name("crash");
    }

    fn disconnect_view_signals(&self) {
        let imp = self.imp();

        // disconnect signals
        GtkUtil::disconnect_signal(imp.decide_policy_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.mouse_over_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.ctx_menu_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.load_progress_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.load_changed_signal.take(), &*imp.view.borrow());
    }

    fn new_webview(ctx: &WebContext, session: &NetworkSession) -> WebView {
        let settings = WebkitSettings::new();
        settings.set_hardware_acceleration_policy(webkit6::HardwareAccelerationPolicy::Always);
        settings.set_enable_html5_database(false);
        settings.set_enable_html5_local_storage(false);
        settings.set_enable_page_cache(false);
        settings.set_enable_smooth_scrolling(true);
        settings.set_enable_javascript(true);
        settings.set_javascript_can_access_clipboard(false);
        settings.set_javascript_can_open_windows_automatically(false);
        settings.set_media_playback_requires_user_gesture(true);
        settings.set_user_agent_with_application_details(Some("Newsflash"), None);
        settings.set_enable_developer_extras(App::default().settings().read().get_inspect_article_view());
        let auto_load = App::default().settings().read().get_article_view_load_images();
        settings.set_auto_load_images(auto_load);

        let webview = Object::builder::<WebView>()
            .property("network-session", session)
            .property("web-context", ctx)
            .build();
        webview.set_settings(&settings);
        Self::load_content_filter(&webview);
        webview
    }

    fn load_content_filter(webview: &WebView) {
        if let Some(user_content_manager) = webview.user_content_manager() {
            if let Some(webkit_data_dir) = WEBKIT_DATA_DIR.to_str() {
                let store = UserContentFilterStore::new(webkit_data_dir);
                let user_content_manager_clone = user_content_manager.clone();

                let css_filter_data =
                    ArticleViewResources::get("stylesheet_filter.json").expect(GTK_RESOURCE_FILE_ERROR);
                let cancellable: Option<&Cancellable> = None;
                store.save(
                    "stylesheetfilter",
                    &glib::Bytes::from_owned(css_filter_data.data),
                    cancellable,
                    move |result| {
                        if let Ok(filter) = result {
                            user_content_manager.add_filter(&filter);
                            log::debug!("Stylesheet filter loaded");
                        } else if let Err(error) = result {
                            log::error!("Failed to load Stylesheet filter: {}", error);
                        }
                    },
                );

                let easylist_adblock_data =
                    ArticleViewResources::get("easylist_min_content_blocker.json").expect(GTK_RESOURCE_FILE_ERROR);
                let cancellable: Option<&Cancellable> = None;
                store.save(
                    "easylistadblock",
                    &glib::Bytes::from_owned(easylist_adblock_data.data),
                    cancellable,
                    move |result| {
                        if let Ok(filter) = result {
                            user_content_manager_clone.add_filter(&filter);
                            log::debug!("Easylist adblock filter loaded");
                        } else if let Err(error) = result {
                            log::error!("Failed to load Easylist adblock filter: {}", error);
                        }
                    },
                );
            }
        }
    }

    pub fn load_user_data(&self) {
        let imp = self.imp();
        if let Some(user_content_manager) = imp.view.borrow().user_content_manager() {
            user_content_manager.remove_all_style_sheets();
            user_content_manager.remove_all_scripts();

            let stylesheet = Self::generate_user_style_sheet();
            user_content_manager.add_style_sheet(&stylesheet);

            let image_dialog_js = Self::generate_img_dialog_js();
            user_content_manager.add_script(&image_dialog_js);
            user_content_manager.register_script_message_handler("imageDialog", None);

            let overshoot_overlay_js = Self::generate_overshoot_overlay_js();
            user_content_manager.add_script(&overshoot_overlay_js);

            let (hightlight_style, hightlight_js) = Self::generate_hightlight_data();
            user_content_manager.add_style_sheet(&hightlight_style);
            user_content_manager.add_script(&hightlight_js);

            user_content_manager.connect_script_message_received(
                None,
                clone!(@weak self as this => @default-panic, move |_manager, js_value| {
                    let imp = this.imp();
                    let image_dialog_visible = imp.image_dialog_visible.clone();

                    // return if an image dialog is already visible
                    if image_dialog_visible.get() {
                        return;
                    }

                    let data_gstr = js_value.to_str();

                    let image_dialog = if data_gstr.as_str().starts_with("data:") {
                        ImageDialog::new_base64_data(data_gstr.as_str())
                    } else if let (Ok(url), Some(article)) =
                        (Url::parse(data_gstr.as_str()), imp.visible_article.borrow().as_ref())
                    {
                        Some(ImageDialog::new_url(&article.article_id, &url))
                    } else {
                        None
                    };

                    if let Some(dialog) = image_dialog {
                        image_dialog_visible.set(true);
                        dialog.connect_close_request(move |_dialog| {
                            image_dialog_visible.set(false);
                            Propagation::Proceed
                        });
                        dialog.present();
                    }
                }),
            );
        }
    }

    fn generate_img_dialog_js() -> UserScript {
        let js_data = ArticleViewResources::get("image_dialog.js").expect(GTK_RESOURCE_FILE_ERROR);
        let js_str = str::from_utf8(js_data.data.as_ref()).expect("Failed to load image_dialog.js css from resources");

        UserScript::new(
            js_str,
            UserContentInjectedFrames::AllFrames,
            UserScriptInjectionTime::Start,
            &[],
            &[],
        )
    }

    fn generate_overshoot_overlay_js() -> UserScript {
        let js_data = ArticleViewResources::get("overshoot_overlay.js").expect(GTK_RESOURCE_FILE_ERROR);
        let js_str =
            str::from_utf8(js_data.data.as_ref()).expect("Failed to load overshoot_overlay.js css from resources");

        UserScript::new(
            js_str,
            UserContentInjectedFrames::AllFrames,
            UserScriptInjectionTime::End,
            &[],
            &[],
        )
    }

    fn generate_hightlight_data() -> (UserStyleSheet, UserScript) {
        let css_data = ArticleViewResources::get("highlight.js/dark.min.css").expect(GTK_RESOURCE_FILE_ERROR);
        let css_str = str::from_utf8(css_data.data.as_ref()).expect("Failed to load hightlight.js css from resources");

        let style = UserStyleSheet::new(
            css_str,
            UserContentInjectedFrames::AllFrames,
            UserStyleLevel::User,
            &[],
            &[],
        );

        let js_data = ArticleViewResources::get("highlight.js/highlight.min.js").expect(GTK_RESOURCE_FILE_ERROR);
        let js_str = str::from_utf8(js_data.data.as_ref()).expect("Failed to load hightlight.js code from resources");

        let script = UserScript::new(
            js_str,
            UserContentInjectedFrames::AllFrames,
            UserScriptInjectionTime::Start,
            &[],
            &[],
        );
        (style, script)
    }

    fn generate_user_style_sheet() -> UserStyleSheet {
        let content_width = App::default().settings().read().get_article_view_width();
        let line_height = App::default().settings().read().get_article_view_line_height();

        let content_width = content_width.unwrap_or(constants::DEFAULT_ARTICLE_CONTENT_WIDTH);
        let line_height = line_height.unwrap_or(constants::DEFAULT_ARTICLE_LINE_HEIGHT);
        let css_data = ArticleViewResources::get("style.css").expect(GTK_RESOURCE_FILE_ERROR);
        let css_string: String = str::from_utf8(css_data.data.as_ref())
            .expect("Failed to load CSS from resources")
            .into();
        let css_string = css_string.replacen("$CONTENT_WIDTH", &format!("{}em", content_width), 1);
        let css_string = css_string.replacen("$LINE_HEIGHT", &format!("{}em", line_height), 1);

        UserStyleSheet::new(
            &css_string,
            UserContentInjectedFrames::TopFrame,
            UserStyleLevel::User,
            &[],
            &[],
        )
    }

    fn connect_event_controller(&self) {
        let imp = self.imp();

        imp.motion_event
            .connect_motion(clone!(@weak self as this => @default-panic, move |_controller, x, y| {
                let imp = this.imp();
                imp.pointer_pos.set((x, y));

                if this.imp().curser_over_link.get() {
                    this.calc_url_overlay_position();
                }
            }));

        //----------------------------------
        // zoom with ctrl+scroll
        //----------------------------------
        imp.scroll_event.connect_scroll(
            clone!(@weak self as this => @default-panic, move |event_controller, _x_delta, y_delta| {
                let imp = this.imp();

                if let Some(event) = event_controller.current_event() {
                    if imp.drag_ongoing.get() {
                        // stop drag if scrolling during drag
                        // https://gitlab.com/news-flash/news_flash_gtk/-/issues/549
                        Self::stop_scroll_animation(&imp.view.borrow(), &imp.scroll_animation_data);
                        imp.drag_ongoing.set(false);
                        imp.drag_y_offset.set(0.0);
                    }

                    if event_controller.current_event_state().contains(ModifierType::CONTROL_MASK) {
                        let zoom = imp.view.borrow().zoom_level();
                        if let Ok(scroll_event) = event.downcast::<ScrollEvent>() {

                            match scroll_event.direction() {
                                ScrollDirection::Up => if zoom >= constants::ARTICLE_ZOOM_UPPER {
                                    return Propagation::Stop;
                                } else {
                                    imp.view.borrow().set_zoom_level(zoom + 0.25);
                                },
                                ScrollDirection::Down => if zoom <= constants::ARTICLE_ZOOM_LOWER {
                                    return Propagation::Stop;
                                } else {
                                    imp.view.borrow().set_zoom_level(zoom - 0.25);
                                },
                                ScrollDirection::Smooth => {
                                    if (y_delta > 0.0 && zoom >= constants::ARTICLE_ZOOM_UPPER) || (y_delta < 0.0 && zoom <= constants::ARTICLE_ZOOM_LOWER) {
                                        return Propagation::Stop;
                                    } else {
                                        let diff = y_delta * constants::ARTICLE_PIXEL_SCROLL_FACTOR;
                                        imp.view.borrow().set_zoom_level(zoom + diff);
                                    }
                                }
                                _ => {}
                            }
                            return Propagation::Stop;
                        }

                        return Propagation::Proceed;
                    }
                }
                Propagation::Proceed
            }),
        );

        //------------------------------------------------
        // zoom with ctrl+PLUS/MINUS & reset with ctrl+0
        //------------------------------------------------
        imp.key_event.connect_key_pressed(
            clone!(@weak self as this => @default-panic, move |_controller, key, _keyval, state| {
                let imp = this.imp();

                if state.contains(ModifierType::CONTROL_MASK) {
                    let zoom = imp.view.borrow().zoom_level();
                    match key {
                        Key::KP_0 | Key::_0  => imp.view.borrow().set_zoom_level(1.0),
                        Key::KP_Add | Key::plus => imp.view.borrow().set_zoom_level(zoom + 0.25),
                        Key::KP_Subtract | Key::minus => imp.view.borrow().set_zoom_level(zoom - 0.25),
                        _ => return Propagation::Proceed,
                    }
                    return Propagation::Stop;
                }
                Propagation::Proceed
            }),
        );

        //------------------------------------------------
        // drag view with middle mouse button
        //------------------------------------------------
        imp.drag_gesture
            .connect_drag_begin(clone!(@weak self as this => @default-panic, move |gesture, _x, _y| {
                let imp = this.imp();

                gesture.set_state(EventSequenceState::Claimed);

                GtkUtil::remove_source(imp.drag_released_motion_signal.take());
                GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
                Self::stop_scroll_animation(&imp.view.borrow(), &imp.scroll_animation_data);
                imp.drag_buffer.replace([0.0; 10]);
                imp.drag_ongoing.set(true);
                imp.drag_momentum.set(0.0);

                imp.drag_buffer_update_signal.replace(
                    Some(glib::timeout_add_local(Duration::from_millis(10), clone!(@weak this => @default-panic, move ||
                    {
                        let imp = this.imp();

                        if !imp.drag_ongoing.get() {
                            imp.drag_buffer_update_signal.take();
                            return ControlFlow::Break;
                        }

                        for i in (1..10).rev() {
                            let value = (*imp.drag_buffer.borrow())[i - 1];
                            (*imp.drag_buffer.borrow_mut())[i] = value;
                        }

                        (*imp.drag_buffer.borrow_mut())[0] = imp.drag_y_offset.get();
                        imp.drag_momentum.set((*imp.drag_buffer.borrow())[9] - (*imp.drag_buffer.borrow())[0]);
                        ControlFlow::Continue
                    })))
                );

            }));

        imp.drag_gesture.connect_drag_update(
            clone!(@weak self as this => @default-panic, move |gesture, _x, y_offset| {
                let imp = this.imp();

                gesture.set_state(EventSequenceState::Claimed);

                if !imp.drag_ongoing.get() {
                    return;
                }

                for i in (1..10).rev() {
                    let value = (*imp.drag_buffer.borrow())[i - 1];
                    (*imp.drag_buffer.borrow_mut())[i] = value;
                }

                (*imp.drag_buffer.borrow_mut())[0] = y_offset;
                imp.drag_momentum.set((*imp.drag_buffer.borrow())[9] - (*imp.drag_buffer.borrow())[0]);

                let scroll = imp.drag_y_offset.get() - y_offset;
                let scroll = scroll / imp.view.borrow().zoom_level();
                imp.drag_y_offset.set(y_offset);
                this.set_scroll_diff(scroll, false);
            }),
        );

        imp.drag_gesture.connect_drag_end(clone!(@weak self as this => @default-panic, move |_gesture, _x, _y| {
            let imp = this.imp();

            if !imp.drag_ongoing.get() {
                return;
            }

            imp.drag_ongoing.set(false);
            imp.drag_y_offset.set(0.0);

            imp.drag_gesture.set_state(EventSequenceState::Claimed);

            let scroll_pos = Rc::new(Cell::new(this.get_scroll_abs()));
            let scroll_upper = this.get_scroll_upper();

            imp.drag_released_motion_signal.replace(
                Some(glib::timeout_add_local(Duration::from_millis(20), clone!(@weak this, @strong scroll_pos => @default-panic, move ||
                {
                    let imp = this.imp();

                    imp.drag_momentum.set(imp.drag_momentum.get() / 1.2);
                    let view_height = imp.view.borrow().height();

                    let page_size = f64::from(view_height);
                    let adjust_value = page_size * imp.drag_momentum.get() / f64::from(view_height);
                    let adjust_value = adjust_value / imp.view.borrow().zoom_level();
                    let old_adjust = scroll_pos.get();
                    let upper = scroll_upper * imp.view.borrow().zoom_level();

                    if (old_adjust + adjust_value) > (upper - page_size)
                        || (old_adjust + adjust_value) < 0.0
                    {
                        imp.drag_momentum.set(0.0);
                    }

                    let new_scroll_pos = f64::min(old_adjust + adjust_value, upper - page_size);
                    scroll_pos.set(new_scroll_pos);
                    this.set_scroll_diff(adjust_value, false);

                    if imp.drag_momentum.get().abs() < 1.0 || imp.drag_ongoing.get() {
                        imp.drag_released_motion_signal.take();
                        return ControlFlow::Break;
                    }

                    ControlFlow::Continue
                })))
            );
        }));
    }

    fn connect_webview(&self) {
        let imp = self.imp();

        //----------------------------------
        // open link in external browser
        //----------------------------------
        imp.decide_policy_signal
            .replace(Some(imp.view.borrow().connect_decide_policy(
                move |_closure_webivew, decision, decision_type| {
                    if decision_type == PolicyDecisionType::NewWindowAction {
                        let navigation_action = decision
                            .downcast_ref::<NavigationPolicyDecision>()
                            .and_then(|descision| descision.navigation_action());

                        if let Some(mut navigation_action) = navigation_action {
                            let is_blank = navigation_action
                                .frame_name()
                                .map(|name| name == "_blank")
                                .unwrap_or(false);
                            if is_blank {
                                if let Some(uri) = navigation_action
                                    .request()
                                    .and_then(|request| request.uri())
                                    .and_then(|uri| Url::parse(uri.as_str()).ok())
                                {
                                    App::default().open_url_in_default_browser(&uri, false);
                                }
                            }
                        }
                        decision.ignore();
                        return true;
                    } else if decision_type == PolicyDecisionType::NavigationAction {
                        let mut action = decision
                            .downcast_ref::<NavigationPolicyDecision>()
                            .and_then(|navigation_decision| navigation_decision.navigation_action());
                        let is_user_gesture = action.as_mut().map(|action| action.is_user_gesture()).unwrap_or(false);

                        if is_user_gesture {
                            if let Some(uri) = action
                                .as_mut()
                                .and_then(|action| action.request())
                                .and_then(|request| request.uri())
                                .and_then(|uri| Url::parse(uri.as_str()).ok())
                            {
                                decision.ignore();
                                App::default().open_url_in_default_browser(&uri, false);
                            }
                        }
                    }
                    false
                },
            )));

        //----------------------------------
        // show url overlay
        //----------------------------------
        imp.mouse_over_signal
            .replace(Some(imp.view.borrow().connect_mouse_target_changed(clone!(
                @weak self as this => @default-panic, move |_closure_webivew, hit_test, _modifiers|
            {
                this.imp().curser_over_link.set(hit_test.context_is_link());
                this.imp().url_overlay_label.set_url(hit_test.link_uri());

                this.calc_url_overlay_position();
            }))));

        //----------------------------------
        // clean up context menu
        //----------------------------------
        imp.ctx_menu_signal.replace(Some(imp.view.borrow().connect_context_menu(
            |_closure_webivew, ctx_menu, hit_test| {
                let menu_items = ctx_menu.items();

                for item in menu_items {
                    if item.is_separator() {
                        ctx_menu.remove(&item);
                        continue;
                    }

                    let keep_stock_actions = [
                        ContextMenuAction::CopyLinkToClipboard,
                        ContextMenuAction::Copy,
                        ContextMenuAction::CopyImageToClipboard,
                        ContextMenuAction::CopyImageUrlToClipboard,
                        ContextMenuAction::InspectElement,
                    ];

                    if !keep_stock_actions.contains(&item.stock_action()) {
                        ctx_menu.remove(&item);
                    }
                }

                if hit_test.context_is_image() {
                    if let Some(save_image_action) = App::default().main_window().lookup_action("save-webview-image") {
                        let image_uri = hit_test.image_uri().unwrap().as_str().to_variant();
                        let save_image_item =
                            ContextMenuItem::from_gaction(&save_image_action, "Save Image", Some(&image_uri));
                        ctx_menu.append(&save_image_item);
                    }
                }

                if hit_test.context_is_link() {
                    if let Some(open_uri_action) = App::default().main_window().lookup_action("open-uri-in-browser") {
                        let uri = hit_test.link_uri().unwrap().as_str().to_variant();
                        let open_uri_item = ContextMenuItem::from_gaction(&open_uri_action, "Open Link", Some(&uri));
                        ctx_menu.insert(&open_uri_item, 0);
                    }
                }

                if ctx_menu.first().is_none() {
                    return true;
                }

                false
            },
        )));

        //----------------------------------
        // display load progress
        //----------------------------------
        imp.load_progress_signal
            .replace(Some(imp.view.borrow().connect_estimated_load_progress_notify(
                clone!(@weak self as this => @default-panic, move |closure_webivew| {
                    let imp = this.imp();

                    if Utc::now() < *imp.load_start_timestamp.borrow() + TimeDelta::try_milliseconds(1500).unwrap() {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }

                    let progress = closure_webivew.estimated_load_progress();
                    if (progress - 1.0).abs() < 0.01 {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }
                    imp.progress_overlay_label.reveal(true);
                    imp.progress_overlay_label.set_percentage(progress);
                }),
            )));

        imp.load_changed_signal
            .replace(Some(imp.view.borrow().connect_load_changed(
                clone!(@weak self as this => @default-panic, move |_closure_webview, event| {
                    let imp = this.imp();
                    GtkUtil::remove_source(imp.unfreeze_insurance_source.take());
                    if event == LoadEvent::Committed && imp.self_stack.is_frozen() {
                        imp.self_stack.update(gtk4::StackTransitionType::Crossfade);
                    }
                }),
            )));

        //----------------------------------
        // crash view
        //----------------------------------
        imp.view.borrow().connect_web_process_terminated(
            clone!(@weak self as this => @default-panic, move |_closure_webivew, _reason|
            {
                this.on_crash();
            }),
        );

        //----------------------------------
        // fullscreen
        //----------------------------------
        imp.view
            .borrow()
            .connect_enter_fullscreen(clone!(@weak self as this => @default-panic, move |_| {
                log::info!("video enter fullscreen");
                let main_window = App::default().main_window();
                let toolbar_view = main_window.content_page().articleview_column().toolbar_view();
                let top = toolbar_view.reveals_top_bars();
                let bottom = toolbar_view.reveals_bottom_bars();

                this.imp().remember_toolbar_state.replace((top, bottom));

                toolbar_view.set_reveal_top_bars(false);
                toolbar_view.set_reveal_bottom_bars(false);
                false
            }));
        imp.view
            .borrow()
            .connect_leave_fullscreen(clone!(@weak self as this => @default-panic, move |_| {
                log::info!("video leave fullscreen");
                let main_window = App::default().main_window();
                    let toolbar_view = main_window.content_page().articleview_column().toolbar_view();
                    let (top, bottom) = this.imp().remember_toolbar_state.take();

                    toolbar_view.set_reveal_top_bars(top);
                    toolbar_view.set_reveal_bottom_bars(bottom);
                false
            }));
    }

    fn build_article(&self, article: &FatArticle, feed_name: &str) -> String {
        let imp = self.imp();

        imp.visible_article_shows_scraped_content.set(
            App::default()
                .content_page_state()
                .borrow()
                .get_prefer_scraped_content(),
        );

        Self::build_article_static(
            article,
            feed_name,
            App::default()
                .content_page_state()
                .borrow()
                .get_prefer_scraped_content(),
        )
    }

    pub fn build_article_static(article: &FatArticle, feed_name: &str, prefer_scraped_content: bool) -> String {
        let settings = App::default().settings();
        let template_data = ArticleViewResources::get(&format!("{}.html", "article")).expect(GTK_RESOURCE_FILE_ERROR);
        let template_str = str::from_utf8(template_data.data.as_ref()).expect(GTK_RESOURCE_FILE_ERROR);
        let mut template_string = template_str.to_owned();

        // A list of fonts we should try to use in order of preference
        // We will pass all of these to CSS in order
        let mut font_options: Vec<String> = Vec::new();
        let mut font_families: Vec<String> = Vec::new();
        let mut font_size: Option<i32> = None;

        // Try to use the configured font if it exists
        if let Some(font_setting) = settings.read().get_article_view_font() {
            font_options.push(font_setting.into());
        }

        // If there is no configured font, or it's broken, use the system default font
        let font_system = App::default().desktop_settings().document_font();
        font_options.push(font_system);

        // Backup if the system font is broken too
        font_options.push("sans".to_owned());

        for font in font_options {
            let desc = FontDescription::from_string(&font);
            if let Some(family) = desc.family() {
                font_families.push(family.to_string());
            }
            if font_size.is_none() && desc.size() > 0 {
                font_size = Some(desc.size());
            }
        }

        // if font size configured use it, otherwise use 12 as default
        let font_size = font_size.unwrap_or(12);

        let font_size = font_size / pango::SCALE;
        let font_family = font_families.join(", ");

        let time = DateUtil::format_time(&article.date);
        let date = DateUtil::format_date(&article.date);
        let formated_date = format!("{date} at {time}");
        let author_date = if let Some(author) = &article.author {
            format!("posted by: {author}, {formated_date}")
        } else {
            formated_date
        };

        // $HTML
        if prefer_scraped_content {
            if let Some(html) = &article.scraped_content {
                template_string = template_string.replacen("$HTML", html, 1);
            } else if let Some(html) = &article.html {
                template_string = template_string.replacen("$HTML", html, 1);
            } else {
                template_string = template_string.replacen("$HTML", constants::NO_CONTENT, 1);
            }
        } else if let Some(html) = &article.html {
            template_string = template_string.replacen("$HTML", html, 1);
        } else {
            template_string = template_string.replacen("$HTML", constants::NO_CONTENT, 1);
        }

        // $AUTHOR / $DATE
        template_string = template_string.replacen("$AUTHOR", &author_date, 1);

        // $SMALLSIZE x2
        let small_size = font_size - 2;
        template_string = template_string.replacen("$SMALLSIZE", &format!("{}", small_size), 2);

        // $TITLE (and URL)
        let mut title = article.title.as_deref().unwrap_or(constants::NO_TITLE).to_owned();
        if let Some(article_url) = &article.url {
            title = format!("<a href=\"{article_url}\" target=\"_blank\">{title}</a>")
        }
        template_string = template_string.replacen("$TITLE", &title, 1);

        // $LARGESIZE
        let large_size = font_size * 2;
        template_string = template_string.replacen("$LARGESIZE", &format!("{}", large_size), 1);

        // $FEED
        template_string = template_string.replacen("$FEED", feed_name, 1);

        // $THEME
        let theme = settings.read().get_article_view_theme().as_str().to_owned();
        template_string = template_string.replacen("$THEME", &theme, 1);

        // $FONTFAMILY
        template_string = template_string.replacen("$FONTFAMILY", &font_family, 1);

        // $FONTSIZE
        template_string = template_string.replacen("$FONTSIZE", &format!("{}", font_size), 1);

        let mut mathjax_string = None;

        if let Some(feed_settings) = settings.read().get_feed_settings(&article.feed_id) {
            // mathjax
            let inline_math = feed_settings
                .inline_math
                .as_ref()
                .map(|inline| format!("inlineMath: [['{}', '{}']]", inline, inline))
                .unwrap_or_default();
            let mathjax_data = ArticleViewResources::get("mathjax/tex-svg.js").expect(GTK_RESOURCE_FILE_ERROR);
            let mathjax_js_str =
                str::from_utf8(mathjax_data.data.as_ref()).expect("Failed to load MATHJAX from resources");
            mathjax_string = Some(format!(
                r#"
                <script>
                    MathJax = {{
                        tex: {{{inline_math}}},
                        svg: {{fontCache: 'global'}}
                    }};
                </script>
                <script id="MathJax-script" async>
                    {mathjax_js_str}
                </script>
            "#
            ));
        }

        template_string = template_string.replacen("$MATHJAX", mathjax_string.as_deref().unwrap_or_default(), 1);

        template_string
    }

    fn scroll_diff_static(view: &WebView, diff: f64, animate: bool) {
        let cancellable: Option<&Cancellable> = None;
        let behavior = if animate { "smooth" } else { "instant" };
        let js = format!(
            "
            window.scrollBy({{
                top: {diff},
                behavior: \"{behavior}\",
            }});"
        );
        view.evaluate_javascript(&js, None, None, cancellable, |res| match res {
            Ok(_) => {}
            Err(_) => error!("Setting scroll pos failed"),
        });
    }

    fn set_scroll_pos_static(view: &WebView, pos: f64, animate: bool) {
        let cancellable: Option<&Cancellable> = None;
        let behavior = if animate { "smooth" } else { "instant" };
        let js = format!(
            "
            window.scrollTo({{
                top: {pos},
                behavior: \"{behavior}\",
            }});"
        );
        view.evaluate_javascript(&js, None, None, cancellable, |res| match res {
            Ok(_) => {}
            Err(_) => error!("Setting scroll pos failed"),
        });
    }

    fn get_scroll_pos_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(view, "window.scrollY").expect("Failed to get scroll position from webview.")
    }

    fn get_scroll_upper_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(
            view,
            "Math.max (
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight,
            document.documentElement.offsetHeight
        )",
        )
        .expect("Failed to get upper limit from webview.")
    }

    fn webview_js_get_f64(view: &WebView, java_script: &str) -> Result<f64> {
        let wait_loop = Rc::new(MainLoop::new(None, false));
        let value: Rc<RefCell<Option<f64>>> = Rc::new(RefCell::new(None));
        let cancellable: Option<&Cancellable> = None;
        view.evaluate_javascript(
            java_script,
            None,
            None,
            cancellable,
            clone!(@weak wait_loop, @weak value => @default-panic, move |res| {
                match res {
                    Ok(result) => {
                        value.replace(Some(result.to_double()));
                    }
                    Err(_) => error!("Getting scroll pos failed"),
                }
                wait_loop.quit();
            }),
        );

        wait_loop.run();

        if let Some(pos) = value.take() {
            Ok(pos)
        } else {
            Err(eyre!("No value from JS"))
        }
    }

    fn set_scroll_diff(&self, diff: f64, animate: bool) {
        let imp = self.imp();
        Self::scroll_diff_static(&imp.view.borrow(), diff, animate);
    }

    fn get_scroll_abs(&self) -> f64 {
        let imp = self.imp();
        Self::get_scroll_pos_static(&imp.view.borrow())
    }

    fn get_scroll_upper(&self) -> f64 {
        let imp = self.imp();
        Self::get_scroll_upper_static(&imp.view.borrow())
    }

    pub fn animate_scroll_diff(&self, diff: f64) {
        let animate = match gtk4::Settings::default() {
            Some(settings) => settings.is_gtk_enable_animations(),
            None => false,
        };

        Self::scroll_diff_static(&self.imp().view.borrow(), diff, self.is_mapped() && animate);
    }

    fn stop_scroll_animation(view: &WebView, properties: &ScrollAnimationProperties) {
        if let Some(callback_id) = properties.scroll_callback_id.take() {
            callback_id.remove();
        }
        view.queue_draw();
        properties.transition_start_value.take();
        properties.transition_diff.take();
        properties.start_time.take();
        properties.end_time.take();
    }

    pub fn clear_cache(&self, oneshot_sender: OneShotSender<()>) {
        let imp = self.imp();
        let data_manager = imp
            .view
            .borrow()
            .network_session()
            .and_then(|session| session.website_data_manager());

        if let Some(data_manager) = data_manager {
            let cancellable: Option<&Cancellable> = None;
            data_manager.clear(
                WebsiteDataTypes::all(),
                glib::TimeSpan::from_seconds(0),
                cancellable,
                move |res| {
                    if let Err(error) = res {
                        log::error!("Failed to clear webkit cache: {}", error);
                    }
                    oneshot_sender.send(()).expect(CHANNEL_ERROR);
                },
            );
        }
    }

    pub fn get_zoom(&self) -> f64 {
        self.imp().view.borrow().zoom_level()
    }

    fn calc_url_overlay_position(&self) {
        let imp = self.imp();

        let overlay_width = imp.url_overlay_label.width();
        let overlay_height =
            imp.url_overlay_label.height() + imp.url_overlay_label.margin_top() + imp.url_overlay_label.margin_bottom();

        if overlay_width == 0 || overlay_height == 0 {
            return;
        }

        let overlay_width = f64::from(overlay_width);
        let overlay_height = f64::from(overlay_height);

        let width = f64::from(imp.stack.width());
        let height = f64::from(imp.stack.height());

        let rel_pointer_x = imp.pointer_pos.get().0 / width;
        let rel_pointer_y = imp.pointer_pos.get().1 / height;

        let rel_label_width = overlay_width / width;
        let rel_label_height = overlay_height / height;

        let can_h_dodge = rel_pointer_x < (1.0 - rel_label_width) || rel_pointer_x > rel_label_width;
        let height_threshold = 1.0 - (rel_label_height * 1.2);

        let v_align = if !can_h_dodge && rel_pointer_y > height_threshold {
            Align::Start
        } else {
            Align::End
        };

        let h_align =
            if v_align == Align::End && rel_pointer_y > height_threshold && rel_pointer_x < (1.0 - rel_label_width) {
                Align::End
            } else {
                Align::Start
            };

        let change_align = imp.url_overlay_label.valign() != v_align || imp.url_overlay_label.halign() != h_align;

        imp.url_overlay_label.set_valign(v_align);
        imp.url_overlay_label.set_halign(h_align);

        if change_align {
            imp.url_overlay_label.queue_resize();
        }
    }
}
