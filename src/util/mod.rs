pub mod constants;
mod date_util;
mod desktop_settings;
mod error;
mod gtk_util;
mod stopwatch;

pub use date_util::DateUtil;
pub use desktop_settings::{ClockFormat, DesktopSettings};
pub use error::UtilError;
use gio::NetworkConnectivity;
pub use gtk_util::GtkUtil;
pub use gtk_util::GTK_RESOURCE_FILE_ERROR;
use parking_lot::RwLock;
use reqwest::{Client, ClientBuilder, Proxy};

use crate::settings::{ProxyModel, ProxyProtocoll, Settings};
use gio::{prelude::ProxyResolverExt, Cancellable, ProxyResolver};
use std::future::Future;
use std::sync::Arc;
use std::time::Duration;

pub const CHANNEL_ERROR: &str = "Error sending message via glib channel";

pub struct Util;

impl Util {
    #[cfg(allow_dead_code)]
    pub fn serialize_and_save<T: serde::Serialize>(object: &T, path: &str) -> Result<String, UtilError> {
        let data = serde_json::to_string_pretty(object).context(UtilErrorKind::Serde)?;
        fs::write(path, &data).context(UtilErrorKind::WriteFile)?;
        Ok(data)
    }

    pub fn glib_spawn_future<F: Future<Output = ()> + 'static>(future: F) {
        glib::MainContext::default().spawn_local(future);
    }

    pub async fn is_online(
        connectivity: NetworkConnectivity,
        network_available: bool,
        client: &Client,
        ping_url: &str,
    ) -> bool {
        log::info!("Networkmonitor connectivity: {connectivity:?} (available {network_available})");

        if network_available && connectivity == NetworkConnectivity::Full {
            return true;
        }

        let res = client.head(ping_url).send().await;

        match res {
            Ok(res) => {
                let status = res.status();
                if status.is_success() {
                    log::info!("pinging '{ping_url}' succeeded");
                } else {
                    log::info!("pinging '{ping_url}' failed: {status:?}");
                }

                status.is_success()
            }
            Err(error) => {
                log::info!("pinging '{ping_url}' failed: {error:?}");
                false
            }
        }
    }

    pub fn discover_gnome_proxy() -> Vec<ProxyModel> {
        let mut proxy_vec = Vec::new();

        let proxy_resolver = ProxyResolver::default();
        let cancellable: Option<&Cancellable> = None;
        if let Ok(proxy_list) = proxy_resolver.lookup("http://example.com/", cancellable) {
            for http_proxy in proxy_list {
                if http_proxy != "direct://" {
                    let url = http_proxy.as_str().to_owned();
                    log::info!("HTTP proxy: '{}'", url);
                    proxy_vec.push(ProxyModel {
                        protocoll: ProxyProtocoll::Http,
                        url,
                        user: None,
                        password: None,
                    });
                }
            }
        }

        if let Ok(proxy_list) = proxy_resolver.lookup("https://example.com/", cancellable) {
            for https_proxy in proxy_list {
                if https_proxy != "direct://" {
                    let url = https_proxy.as_str().to_owned();
                    log::info!("HTTPS proxy: '{}'", url);
                    proxy_vec.push(ProxyModel {
                        protocoll: ProxyProtocoll::Https,
                        url,
                        user: None,
                        password: None,
                    });
                }
            }
        }

        proxy_vec
    }

    pub fn symbolic_icon_set_color(data: &[u8], hex_color: &str) -> Result<Vec<u8>, UtilError> {
        let svg_string = std::str::from_utf8(data).map_err(|_| UtilError::Svg)?;
        let colored_svg_string = svg_string.replace("fill:#bebebe", &format!("fill:{}", hex_color));
        Ok(colored_svg_string.as_bytes().into())
    }

    pub fn format_data_size(bytes: u64) -> String {
        let step = 1024.0;
        let kbytes = bytes as f64 / step;
        let mbytes = kbytes / step;
        let gbytes = mbytes / step;

        if bytes < 1024 {
            format!("{} bytes", bytes)
        } else if kbytes < step {
            format!("{:.1} kb", kbytes)
        } else if mbytes < step {
            format!("{:.1} mb", mbytes)
        } else {
            format!("{:.1} gb", gbytes)
        }
    }

    pub fn build_client(settings: Arc<RwLock<Settings>>) -> Client {
        let mut builder = ClientBuilder::new()
            .user_agent("Mozilla/5.0 (X11; Linux x86_64; rv:122.0)")
            .use_native_tls()
            .trust_dns(false)
            .gzip(true)
            .brotli(true)
            .timeout(Duration::from_secs(60))
            .danger_accept_invalid_certs(settings.read().get_accept_invalid_certs())
            .danger_accept_invalid_hostnames(settings.read().get_accept_invalid_hostnames());

        let mut proxys = settings.read().get_proxy();
        proxys.append(&mut Util::discover_gnome_proxy());

        for proxy_model in proxys {
            if proxy_model.url.starts_with("socks4") {
                continue;
            }

            let mut proxy = match &proxy_model.protocoll {
                ProxyProtocoll::All => Proxy::all(&proxy_model.url),
                ProxyProtocoll::Http => Proxy::http(&proxy_model.url),
                ProxyProtocoll::Https => Proxy::https(&proxy_model.url),
            }
            .unwrap_or_else(|_| panic!("Failed to build proxy: {}", proxy_model.url));

            if let Some(proxy_user) = &proxy_model.user {
                if let Some(proxy_password) = &proxy_model.password {
                    proxy = proxy.basic_auth(proxy_user, proxy_password);
                }
            }

            builder = builder.proxy(proxy);
        }

        builder.build().expect("Failed to build reqwest client")
    }
}
