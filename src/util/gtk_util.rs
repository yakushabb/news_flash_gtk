use super::constants;
use super::error::UtilError;
use crate::color::ColorRGBA;
use gdk4::prelude::*;
use gdk4::Display;
use gio::{Cancellable, Resource};
use glib::{object::ObjectExt, signal::SignalHandlerId, source::SourceId, Bytes};
use gtk4::cairo::{Context, FillRule, LinearGradient};
use gtk4::IconTheme;
use libadwaita::{prelude::*, EntryRow};
use rust_embed::RustEmbed;

#[derive(RustEmbed)]
#[folder = "data/resources/gresource_bundles"]
struct GResources;

pub const GTK_RESOURCE_FILE_ERROR: &str = "Could not load file from resources. This should never happen!";

pub struct GtkUtil;

impl GtkUtil {
    pub fn register_symbolic_icons() {
        let data = GResources::get("icons.gresource").expect(GTK_RESOURCE_FILE_ERROR);
        let bytes = Bytes::from(&data.data);
        let icon_resource = Resource::from_data(&bytes).expect("Error creating gio resource.");
        gio::resources_register(&icon_resource);
        if let Some(display) = Display::default() {
            IconTheme::for_display(&display).add_resource_path("/com/gitlab/news_flash/icons/");
        }
    }

    pub fn register_styles() {
        let data = GResources::get("styles.gresource").expect(GTK_RESOURCE_FILE_ERROR);
        let bytes = Bytes::from(&data.data);
        let styles_resource = Resource::from_data(&bytes).expect("Error creating gio resource.");
        gio::resources_register(&styles_resource);
    }

    pub fn is_entry_row_emty(entry: &EntryRow) -> bool {
        entry.text().as_str().is_empty()
    }

    pub fn disconnect_signal<T: ObjectExt>(signal_id: Option<SignalHandlerId>, widget: &T) {
        if let Some(signal_id) = signal_id {
            widget.disconnect(signal_id);
        }
    }

    pub fn remove_source(source_id: Option<SourceId>) {
        if let Some(source_id) = source_id {
            source_id.remove();
        }
    }

    pub fn adjust_lightness(color: &ColorRGBA, adjust: f64, fallback: Option<&ColorRGBA>) -> ColorRGBA {
        let mut color = *color;
        if color.adjust_lightness(adjust).is_err() {
            if let Some(fallback) = fallback {
                *fallback
            } else {
                ColorRGBA::from_normalized(0.5, 0.5, 0.5, 1.0)
            }
        } else {
            color
        }
    }

    pub fn draw_color_cirlce(cairo_ctx: &Context, color: &str, center: Option<(f64, f64)>) {
        let size = 16_f64;
        let half_size = size / 2_f64;

        let tag_color = match ColorRGBA::parse_string(color) {
            Ok(color) => color,
            Err(_) => ColorRGBA::parse_string(constants::TAG_DEFAULT_COLOR)
                .expect("Failed to parse default outer RGBA string."),
        };
        let gradient_upper = Self::adjust_lightness(&tag_color, constants::TAG_GRADIENT_SHIFT, None);
        let gradient_lower = Self::adjust_lightness(&tag_color, -constants::TAG_GRADIENT_SHIFT, None);

        cairo_ctx.set_fill_rule(FillRule::EvenOdd);

        let gradient = LinearGradient::new(half_size, 0.0, half_size, size);
        gradient.add_color_stop_rgb(
            0.0,
            gradient_upper.red_normalized(),
            gradient_upper.green_normalized(),
            gradient_upper.blue_normalized(),
        );
        gradient.add_color_stop_rgb(
            1.0,
            gradient_lower.red_normalized(),
            gradient_lower.green_normalized(),
            gradient_lower.blue_normalized(),
        );

        cairo_ctx
            .set_source(&gradient)
            .expect("Failed to set gradient as ctx source");
        let center_x = center.map(|c| c.0).unwrap_or(half_size);
        let center_y = center.map(|c| c.1).unwrap_or(half_size);
        cairo_ctx.arc(center_x, center_y, half_size, 0.0, 2.0 * std::f64::consts::PI);
        cairo_ctx.fill().expect("Failed cairo fill");
    }

    pub fn read_bytes_from_file(file: &gio::File) -> Result<Vec<u8>, UtilError> {
        let (bytes, _) = file.load_bytes(None::<&Cancellable>)?;

        Ok(bytes.to_vec())
    }

    pub fn write_bytes_to_file(bytes: &[u8], file: &gio::File) -> Result<(), UtilError> {
        if file.query_exists(None::<&Cancellable>) {
            let stream = file.open_readwrite(None::<&Cancellable>)?;
            stream.output_stream().write_all(bytes, None::<&Cancellable>)?;
        } else {
            let stream = file.create(gio::FileCreateFlags::REPLACE_DESTINATION, None::<&Cancellable>)?;
            stream.write_all(bytes, None::<&Cancellable>)?;
        }

        Ok(())
    }
}
