use glib::Boxed;
use news_flash::models::Feed;

#[derive(Clone, Debug, Boxed)]
#[boxed_type(name = "NewsFlashGFeed")]
pub struct GFeed(Feed);

impl From<Feed> for GFeed {
    fn from(f: Feed) -> Self {
        Self(f)
    }
}

impl From<GFeed> for Feed {
    fn from(f: GFeed) -> Self {
        f.0
    }
}
