use crate::app::App;
use crate::edit_feed_dialog::CategorySelectGObject;
use crate::i18n::i18n;
use gio::ListStore;
use glib::subclass;
use glib::{clone, subclass::*};
use gtk4::PropertyExpression;
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Image, Widget};
use libadwaita::prelude::*;
use libadwaita::{ComboRow, EntryRow};
use news_flash::models::{Category, CategoryID, Feed, Url, NEWSFLASH_TOPLEVEL};
use once_cell::sync::Lazy;

use std::cell::RefCell;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/feed_widget.blp")]
    pub struct AddFeedWidget {
        #[template_child]
        pub feed_title_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub favicon_image: TemplateChild<Image>,
        #[template_child]
        pub add_button: TemplateChild<Button>,
        #[template_child]
        pub category_combo: TemplateChild<ComboRow>,

        pub feed_url: Rc<RefCell<Option<Url>>>,
        pub feed_category: RefCell<Option<CategoryID>>,
    }

    impl Default for AddFeedWidget {
        fn default() -> Self {
            AddFeedWidget {
                feed_title_entry: TemplateChild::default(),
                favicon_image: TemplateChild::default(),
                add_button: TemplateChild::default(),
                category_combo: TemplateChild::default(),

                feed_url: Rc::new(RefCell::new(None)),
                feed_category: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddFeedWidget {
        const NAME: &'static str = "AddFeedWidget";
        type ParentType = Box;
        type Type = super::AddFeedWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddFeedWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("feed-added").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddFeedWidget {}

    impl BoxImpl for AddFeedWidget {}
}

glib::wrapper! {
    pub struct AddFeedWidget(ObjectSubclass<imp::AddFeedWidget>)
        @extends Widget, Box;
}

impl Default for AddFeedWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddFeedWidget {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        self.setup_category_selector();

        let imp = self.imp();
        let add_button = imp.add_button.get();
        imp.feed_title_entry
            .connect_changed(clone!(@weak add_button => @default-panic, move |entry| {
                add_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        imp.add_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let feed_url = match imp.feed_url.borrow().clone() {
                Some(url) => url,
                None => {
                    log::error!("Failed to add feed: No valid url");
                    App::default().in_app_notifiaction(&i18n("Failed to add feed: No valid url"));
                    return;
                }
            };
            let feed_title = if imp.feed_title_entry.text().as_str().is_empty() { None } else { Some(imp.feed_title_entry.text().as_str().into()) };

            App::default().add_feed(feed_url, feed_title, imp.feed_category.borrow().clone());
            widget.emit_by_name::<()>("feed-added", &[]);
        }));
    }

    fn setup_category_selector(&self) {
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    if let Ok((categories, _mappings)) = news_flash.get_categories() {
                        return categories;
                    }
                }

                Vec::new()
            },
            clone!(@weak self as this => @default-panic, move |_app, categories: Vec<Category>| {
                let imp = this.imp();

                let list_store = ListStore::new::<CategorySelectGObject>();
                list_store.append(&CategorySelectGObject::new(&NEWSFLASH_TOPLEVEL, &i18n("None")));
                for category in &categories {
                    list_store.append(&CategorySelectGObject::from_category(category));
                }

                let expression = PropertyExpression::new(
                    CategorySelectGObject::static_type(),
                    None::<&PropertyExpression>,
                    "label",
                );
                imp.category_combo.set_expression(Some(&expression));
                imp.category_combo.set_model(Some(&list_store));
                imp.category_combo.set_selected(0);

                imp.category_combo
                    .connect_selected_item_notify(clone!(@weak this => @default-panic, move |combo| {
                        if let Some(selected) = combo
                            .selected_item()
                            .and_then(|obj| obj.downcast::<CategorySelectGObject>().ok())
                        {
                            let imp = this.imp();
                            let mut category_id = None;
                            if *selected.id() != *NEWSFLASH_TOPLEVEL {
                                category_id = Some(selected.id().clone());
                            }
                            imp.feed_category.replace(category_id);
                        }
                    }));
            }),
        );
    }

    pub fn reset(&self) {
        let imp = self.imp();
        imp.feed_title_entry.set_text("");
        imp.feed_url.take();
        imp.favicon_image.set_from_paintable(None::<&gdk4::Paintable>);
    }

    pub fn fill(&self, feed: Feed) {
        let imp = self.imp();

        imp.feed_title_entry.set_text(&feed.label);
        if let Some(new_feed_url) = &feed.feed_url {
            imp.feed_url.replace(Some(new_feed_url.clone()));
        } else {
            imp.feed_url.take();
        }

        let favicon_image = imp.favicon_image.get();
        let feed_clone = feed;
        App::default().execute_with_callback(
            |_news_flash, client| async move {
                news_flash::util::favicon_cache::FavIconCache::fetch_new_icon(
                    None,
                    &feed_clone,
                    &client,
                    None,
                    Some(128), // image is 64x64 so try to fetch a 128px icon because of 2x scaling
                )
                .await
            },
            move |_app, favicon| {
                if let Some(data) = favicon.data {
                    let bytes = glib::Bytes::from_owned(data);
                    if let Ok(texture) = gdk4::Texture::from_bytes(&bytes) {
                        favicon_image.set_from_paintable(Some(&texture));
                    }
                }
            },
        );
    }
}
