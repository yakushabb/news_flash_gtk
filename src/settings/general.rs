use crate::i18n::i18n;
use chrono::TimeDelta;
use glib::Enum;
use serde::{Deserialize, Serialize};
use std::default::Default;
use std::fmt;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq)]
pub enum SyncInterval {
    Never,
    Predefined(PredefinedSyncInterval),
    Custom(u32), // Seconds
}

impl SyncInterval {
    pub fn as_minutes(&self) -> Option<u32> {
        match self {
            Self::Never => None,
            Self::Predefined(pre) => pre.as_minutes(),
            Self::Custom(duration) => Some(duration / 60),
        }
    }

    pub fn as_seconds(&self) -> Option<u32> {
        match self {
            Self::Never => None,
            Self::Predefined(pre) => pre.as_seconds(),
            Self::Custom(duration) => Some(*duration),
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "SyncInterval")]
pub enum PredefinedSyncInterval {
    QuaterHour,
    HalfHour,
    Hour,
    TwoHour,
}

impl fmt::Display for PredefinedSyncInterval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::QuaterHour => write!(f, "{}", i18n("15 Minutes")),
            Self::HalfHour => write!(f, "{}", i18n("30 Minutes")),
            Self::Hour => write!(f, "{}", i18n("1 Hour")),
            Self::TwoHour => write!(f, "{}", i18n("2 Hours")),
        }
    }
}

impl PredefinedSyncInterval {
    pub fn as_minutes(&self) -> Option<u32> {
        match self {
            Self::QuaterHour => Some(15),
            Self::HalfHour => Some(30),
            Self::Hour => Some(60),
            Self::TwoHour => Some(120),
        }
    }

    pub fn as_seconds(&self) -> Option<u32> {
        self.as_minutes().map(|m| m * 60)
    }

    pub fn from_u32(v: u32) -> Self {
        match v {
            0 => Self::QuaterHour,
            1 => Self::HalfHour,
            2 => Self::Hour,
            3 => Self::TwoHour,
            _ => Self::QuaterHour,
        }
    }

    pub fn as_u32(&self) -> u32 {
        match self {
            Self::QuaterHour => 0,
            Self::HalfHour => 1,
            Self::Hour => 2,
            Self::TwoHour => 3,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "KeepArticlesDuration")]
pub enum KeepArticlesDuration {
    Forever,
    OneYear,
    SixMonths,
    OneMonth,
    OneWeek,
}

impl fmt::Display for KeepArticlesDuration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Forever => write!(f, "{}", i18n("Forever")),
            Self::OneYear => write!(f, "{}", i18n("One Year")),
            Self::SixMonths => write!(f, "{}", i18n("6 Months")),
            Self::OneMonth => write!(f, "{}", i18n("One Month")),
            Self::OneWeek => write!(f, "{}", i18n("One Week")),
        }
    }
}

impl KeepArticlesDuration {
    pub fn as_duration(&self) -> Option<TimeDelta> {
        match self {
            Self::Forever => None,
            Self::OneYear => Some(TimeDelta::try_days(365).unwrap()),
            Self::SixMonths => Some(TimeDelta::try_days(182).unwrap()),
            Self::OneMonth => Some(TimeDelta::try_days(30).unwrap()),
            Self::OneWeek => Some(TimeDelta::try_days(7).unwrap()),
        }
    }

    pub fn from_duration(duration: Option<TimeDelta>) -> Self {
        if let Some(duration) = duration {
            match duration.num_days() {
                365 => Self::OneYear,
                182 => Self::SixMonths,
                30 => Self::OneMonth,
                7 => Self::OneWeek,

                _ => Self::Forever,
            }
        } else {
            KeepArticlesDuration::Forever
        }
    }

    pub fn as_u32(&self) -> u32 {
        match self {
            Self::Forever => 0,
            Self::OneYear => 1,
            Self::SixMonths => 2,
            Self::OneMonth => 3,
            Self::OneWeek => 4,
        }
    }

    pub fn from_u32(i: u32) -> Self {
        match i {
            0 => Self::Forever,
            1 => Self::OneYear,
            2 => Self::SixMonths,
            3 => Self::OneMonth,
            4 => Self::OneWeek,

            _ => Self::Forever,
        }
    }
}

const fn _default_true() -> bool {
    true
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GeneralSettings {
    #[serde(default)]
    pub keep_running_in_background: bool,
    #[serde(default)]
    pub autostart: bool,
    #[serde(default)]
    pub sync_on_startup: bool,
    #[serde(default = "_default_true")]
    pub sync_on_metered: bool,
    pub sync_every: SyncInterval,
}

impl Default for GeneralSettings {
    fn default() -> Self {
        GeneralSettings {
            keep_running_in_background: false,
            autostart: false,
            sync_on_startup: false,
            sync_on_metered: true,
            sync_every: SyncInterval::Predefined(PredefinedSyncInterval::QuaterHour),
        }
    }
}
