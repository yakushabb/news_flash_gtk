mod app_page;
mod keybinding_editor;
mod share_page;
mod shortcuts_page;
mod theme_chooser;
mod views_page;

use self::{
    app_page::SettingsAppPage, share_page::SettingsSharePage, shortcuts_page::SettingsShortcutsPage,
    views_page::SettingsViewsPage,
};

use glib::prelude::*;
use glib::subclass;
use gtk4::prelude::GtkWindowExt;
use gtk4::subclass::prelude::*;
use gtk4::{CompositeTemplate, Widget, Window};
use libadwaita::subclass::prelude::*;
use libadwaita::{PreferencesWindow, Window as AdwWindow};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/settings/dialog.blp")]
    pub struct SettingsDialog {
        #[template_child]
        pub views_page: TemplateChild<SettingsViewsPage>,
        #[template_child]
        pub app_page: TemplateChild<SettingsAppPage>,
        #[template_child]
        pub shortcuts_page: TemplateChild<SettingsShortcutsPage>,
        #[template_child]
        pub share_page: TemplateChild<SettingsSharePage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsDialog {
        const NAME: &'static str = "SettingsDialog";
        type ParentType = PreferencesWindow;
        type Type = super::SettingsDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsDialog {}

    impl WidgetImpl for SettingsDialog {}

    impl WindowImpl for SettingsDialog {}

    impl AdwWindowImpl for SettingsDialog {}

    impl PreferencesWindowImpl for SettingsDialog {}
}

glib::wrapper! {
    pub struct SettingsDialog(ObjectSubclass<imp::SettingsDialog>)
        @extends Widget, Window, AdwWindow, PreferencesWindow;
}

impl Default for SettingsDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsDialog {
    pub fn new<W: IsA<Window> + GtkWindowExt>(parent: &W) -> Self {
        let dialog = Self::default();
        dialog.set_transient_for(Some(parent));
        dialog.init();
        dialog
    }

    fn init(&self) {
        let imp = self.imp();
        imp.shortcuts_page.init(self);
    }
}
