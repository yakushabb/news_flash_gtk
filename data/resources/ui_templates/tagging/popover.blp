using Gtk 4.0;
using Gio 2.0;

template $TagPopover : Popover {
  child: Box {
    orientation: vertical;
    spacing: 10;
    margin-start: 2;
    margin-end: 2;
    margin-top: 2;
    margin-bottom: 2;

    Stack stack {
      transition-type: crossfade;
      interpolate-size: true;
      vexpand: true;

      StackPage {
        name: "empty";
        child: Box {
          orientation: vertical;
          valign: center;
          spacing: 5;
          margin-start: 15;
          margin-end: 15;
          margin-top: 15;
          margin-bottom: 15;

          Label {
            label: _("Add Tags");

            styles [
              "large-title",
            ]
          }

          Label {
            label: _("Type to create a new Tag");

            styles [
              "dim-label",
            ]
          }
        };
      }

      StackPage {
        name: "create";
        child: Box {
          orientation: vertical;
          valign: center;
          spacing: 5;
          margin-start: 15;
          margin-end: 15;
          margin-top: 15;
          margin-bottom: 15;

          Label {
            label: _("Create Tag");

            styles [
              "large-title",
            ]
          }

          Label create_label {
            wrap: true;
            wrap-mode: word;
            ellipsize: end;
            lines: 2;
            max-width-chars: 25;
            justify: center;

            styles [
              "dim-label",
            ]
          }
        };
      }

      StackPage {
        name: "list";
        child: Box {
          orientation: vertical;
          spacing: 10;

          Box {
            orientation: vertical;
            spacing: 5;
            margin-start: 15;
            margin-end: 15;
            margin-top: 15;
            margin-bottom: 15;

            Label {
              label: _("Tag the Article");

              styles [
                "large-title",
              ]
            }

            Label {
              wrap: true;
              wrap-mode: word;
              ellipsize: end;
              lines: 2;
              max-width-chars: 28;
              justify: center;
              label: _("Press enter to assign a Tag or ctrl + enter to create a new Tag");

              styles [
                "dim-label",
              ]
            }
          }

          ScrolledWindow scroll {
            min-content-height: 116;

            ListView listview {
              single-click-activate: true;
              show-separators: true;
              enable-rubberband: false;
              model: 
              SingleSelection selection {
                model: SortListModel sort_list {
                  model: FilterListModel filter_list {
                    model: Gio.ListStore list_store {};
                    filter: StringFilter filter {};
                  };
                  sorter: CustomSorter sorter {};
                };
              };
              factory: SignalListItemFactory factory {};

              styles [
                "card",
              ]
            }
          }
        };
      }
    }

    SearchEntry entry {
      placeholder-text: _("Search and Create Tags");

      EventControllerKey key_event {
        propagation-phase: capture;
      }
    }
  };
}
